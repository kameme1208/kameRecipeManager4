package com.gitlab.kameme1208.kameRecipeManager.utils.display;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public abstract class TableNameTag {

	protected Set<Player> players = new HashSet<>();

	public static TableNameTag create(Block block, String name) {
		return new v1_12_R1(block, name);
	}

	public void destroy() {
		players.removeIf(x->!x.isOnline());
		players.forEach(this::hide);
	}

	public abstract void moveTo(Block block);
	public abstract void hide(Player player);
	public abstract void show(Player player);
	public abstract void ride(Entity entity);


	private static class v1_12_R1 extends TableNameTag {
		private net.minecraft.server.v1_12_R1.EntityArmorStand stand;

		private v1_12_R1(Block block, String name) {
			double x = block.getX() + 0.5;
			double y = block.getY() + 1.0;
			double z = block.getZ() + 0.5;
			stand = new net.minecraft.server.v1_12_R1.EntityArmorStand(((org.bukkit.craftbukkit.v1_12_R1.CraftWorld)block.getWorld()).getHandle(), x, y, z);
			stand.setInvisible(true);
			stand.setMarker(true);
			if(name != null) {
				stand.setCustomName(name);
				stand.setCustomNameVisible(true);
			}
			//stand.setFlag(0, true);
		}

		public void moveTo(Block block) {
			players.removeIf(x->!x.isOnline());
			stand.locX = block.getX() + 0.5;
			stand.locY = block.getY() + 1.0;
			stand.locZ = block.getZ() + 0.5;
			net.minecraft.server.v1_12_R1.Packet<?> packet = new net.minecraft.server.v1_12_R1.PacketPlayOutEntityTeleport(stand);
			players.forEach(x -> ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer)x).getHandle().playerConnection.sendPacket(packet));
		}

		@Override
		public void hide(Player player) {
			players.removeIf(x->!x.isOnline());
			net.minecraft.server.v1_12_R1.Packet<?> packet = new net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy(stand.getId());
			((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
			players.remove(player);
		}

		@Override
		public void show(Player player) {
			players.removeIf(x->!x.isOnline());
			net.minecraft.server.v1_12_R1.Packet<?> packet = new net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving(stand);
			((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
			players.add(player);
		}

		public void ride(Entity entity) {
			players.removeIf(x->!x.isOnline());
			net.minecraft.server.v1_12_R1.Entity e = ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity)entity).getHandle();
			e.passengers.add(stand);
			net.minecraft.server.v1_12_R1.Packet<?> packet = new net.minecraft.server.v1_12_R1.PacketPlayOutAttachEntity(stand, e);
			players.forEach(x -> ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer)x).getHandle().playerConnection.sendPacket(packet));
		}

	}
}
