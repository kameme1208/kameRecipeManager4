package com.gitlab.kameme1208.kameRecipeManager.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

public class Utils {

	public static int toInt(String input, int def) {
		try {
			return Integer.parseInt(input);
		}catch(NumberFormatException e) {
			return def;
		}
	}

	public static void reloaded(Player player) {
		player.sendMessage(Lang.reloaded());
		player.closeInventory();
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
	}

	private static List<ItemStack> replaceMeta(Collection<ItemStack> item, Entity entity, Location loc) {
		item.forEach(x -> replaceMeta(x, entity, loc));
		return Lists.newArrayList(item);
	}

	public static ItemStack replaceMeta(ItemStack item, Entity entity, Location loc) {
		if(item == null || !item.hasItemMeta())return item;
		ItemMeta im = item.getItemMeta();
		if(im.hasDisplayName())im.setDisplayName(kame.kameplayer.baseutils.Utils.engine(im.getDisplayName(), entity, loc, entity));
		if(im.hasLore()) {
			List<String> lore = im.getLore();
			List<String> output = new ArrayList<>();
			for(String line : lore) {
				output.add(kame.kameplayer.baseutils.Utils.engine(line, entity, loc, entity));
			}
			im.setLore(output);
		}
		item = item.clone();
		item.setItemMeta(im);
		return item;
	}
}
