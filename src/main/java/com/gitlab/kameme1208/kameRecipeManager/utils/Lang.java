package com.gitlab.kameme1208.kameRecipeManager.utils;

import org.bukkit.ChatColor;

import com.gitlab.kameme1208.kameRecipeManager.Main;

public class Lang {
	private static String replace(String line) {

		return line != null ? ChatColor.translateAlternateColorCodes('&', line) : "";
	}
	public static String crafttable() {
		return replace(Main.config.getString("display.crafttable"));
	}

	public static String furnace() {
		return replace(Main.config.getString("display.furnace"));
	}

	public static String anvil() {
		return replace(Main.config.getString("display.anvil"));
	}

	public static String brewer() {
		return replace(Main.config.getString("display.brewstand"));
	}

	public static String list() {
		return replace(Main.config.getString("display.recipeList"));
	}

	public static String keepDisp(boolean bool) {
		return replace(bool ? Main.config.getString("message.DispKeepOn") : Main.config.getString("message.DispKeepOff"));
	}

	public static String anvilusing() {
		return replace(Main.config.getString("message.Anvil-using"));
	}

	public static String unknownError() {
		return replace(Main.config.getString("message.UnknownError"));
	}

	public static String reloaded() {
		return replace(Main.config.getString("message.Inventory-reloaded"));
	}

	public static String anvilSpel() {
		return replace(Main.config.getString("message.Anvil-Enterspell"));
	}

	public static String changename() {
		return replace(Main.config.getString("message.FailedChangeName"));
	}
}
