package com.gitlab.kameme1208.kameRecipeManager.utils.display;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.gitlab.kameme1208.kameRecipeManager.recipe.Recipes;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapedRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapedRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.utils.Lang;

public class WorkbenchDisplay implements Display {

	private Player player;
	private Scoreboard score;
	@Override
	public void hideScore(int later) {
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
			if(player.getScoreboard() == score)player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
			score = null;
		}, later);
	}
	
	@Override
	public boolean update(String name, int score) {
		return true;
	}
	
	public WorkbenchDisplay(Block block, Player player) {
		this.player = player;
		Set<String> include = TableData.fromBlock(block);
		if(include.isEmpty())include.add(Lang.crafttable());
		Map<Class<? extends CustomRecipe>, TreeMap<String, List<CustomRecipe>>> recipes = Recipes.getSortedAllRecipes();
		int size = 0;
		List<Map<String, List<CustomRecipe>>> maps = Arrays.asList(
				recipes.getOrDefault(CustomShapedRecipe.class, new TreeMap<>()),
				recipes.getOrDefault(VanillaShapedRecipe.class, new TreeMap<>()),
				recipes.getOrDefault(CustomShapelessRecipe.class, new TreeMap<>()),
				recipes.getOrDefault(VanillaShapelessRecipe.class, new TreeMap<>()));
		
		for(String str : include) {
			for(Map<String, List<CustomRecipe>> map : maps) {
				size += map.getOrDefault(str, Collections.emptyList()).size();
			}
		}
		
		score = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = score.registerNewObjective(include.iterator().next(), include.iterator().next());
		obj.getScore(Lang.list()).setScore(size);;
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		player.setScoreboard(score);
		
	}
}
