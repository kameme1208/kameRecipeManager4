package com.gitlab.kameme1208.kameRecipeManager.utils.display;

public interface Display {

	public boolean update(String name, int score);

	public void hideScore(int later);

}
