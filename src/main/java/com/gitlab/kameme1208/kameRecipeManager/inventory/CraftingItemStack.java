package com.gitlab.kameme1208.kameRecipeManager.inventory;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class CraftingItemStack extends ItemStack {
	public boolean keep;

	public CraftingItemStack(Material material, int amount, int durability) {
		super(material, amount, (short)durability);
	}

	public void setKeep(boolean keep) {
		this.keep = keep;
	}

	public boolean getKeep() {
		return keep;
	}

	@Override
	public CraftingItemStack clone() {
		CraftingItemStack item = (CraftingItemStack)super.clone();
		item.keep = this.keep;

		return item;
	}

}
