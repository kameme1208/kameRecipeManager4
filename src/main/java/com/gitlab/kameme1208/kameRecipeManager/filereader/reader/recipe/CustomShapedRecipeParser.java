package com.gitlab.kameme1208.kameRecipeManager.filereader.reader.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeParser;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapedRecipe;

public class CustomShapedRecipeParser extends RecipeParser<CustomShapedRecipe> {

	private ArrayList<String> shapes = new ArrayList<>();
	private Map<Integer, ItemStack> rawmap = new HashMap<>();
	private int sizeX = 0;
	private int sizeY = 0;

	public CustomShapedRecipeParser(Plugin plugin, String recipename, String key) {
		super(plugin, recipename, key);
	}

	private boolean addShape(String shape) {
		if(shape.length() > 3)return false;
		sizeX = Math.max(sizeX, shape.length());
		if(sizeY < 3)sizeY++;
		return shapes.size() < 3 ? shapes.add(shape) : false;
	}

	private boolean setItem(int key, ItemStack item) {
		return rawmap.containsKey(key) ? false : rawmap.put(key, item) == null;
	}

	@Override
	public void parseLine(String line) throws RecipeParseException {
		if(line.matches("[1-9] = .*")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), ParseStage.INPUT);
			if(setItem(line.charAt(0) - 48, item))throw new ItemStateException("InputItemError:");
		}else if(line.matches("[0-9]( *[0-9])?( *[0-9])? *")) {
			if(!this.addShape(line.replaceAll("[^0-9]", "")))throw new ItemStateException("InputItemError:");
		}
	}

	@Override
	public boolean isReady() {
		return rawmap.size() != 0 && shapes.size() != 0 && getResult() != null && !dorpped;
	}

	@Override
	public Collection<CustomShapedRecipe> createRecipe() {
		super.buildProducts();
		Map<Character, ItemStack> items = new HashMap<>();
		String[] shape = new String[sizeY];
		char c = 'a';
		for(int i = 0; i < sizeY; i++) {
			char[] ch = new char[sizeX];
			for(int j = 0; j < sizeX; j++) {
				
				ItemStack item = null;
				if(shapes.size() > i && shapes.get(i).length() > j) {
					item = rawmap.get(shapes.get(i).charAt(j) - '0');
				}
				if(item != null)items.put(c, item);
				ch[j] = c;
				c++;
			}
			shape[i] = new String(ch);
		}
		List<CustomShapedRecipe> recipe = new ArrayList<>();
		recipe.add(new CustomShapedRecipe(getKey(), shape, items, getResult(), getOptions(), getCheck(), getProduct()));

		Map<Character, ItemStack> item2 = new HashMap<>(items);
		if(sizeX == 1)return recipe;
		for(int i = 0; i < sizeY; i++) {
			int a = sizeX * i;//0 2 4
			item2.put((char)('a' + a), item2.put((char)('a' + sizeX - 1 + a), item2.get((char)('a' + a))));
		}
		if(item2.equals(items))return recipe;
		recipe.add(new CustomShapedRecipe(getKey(), shape, item2, getResult(), getOptions(), getCheck(), getProduct()));
		return recipe;
	}

	@Override
	public String getCause() {
		if(shapes.size() == 0)return "Shape is not set! (0~9 + 0~9 + 0~9)";
		if(rawmap.size() == 0)return "Input item is empty! ([1-9] = Items)";
		if(getResult()== null)return "Output item is not set! (=== Items)";
		return "";
	}

}
