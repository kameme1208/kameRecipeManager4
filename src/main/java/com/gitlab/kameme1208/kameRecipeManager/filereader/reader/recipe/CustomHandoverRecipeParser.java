package com.gitlab.kameme1208.kameRecipeManager.filereader.reader.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemAmountException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.NumberParseException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeParser;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomHandoverRecipe;
import com.google.common.collect.Lists;

import kame.api.nbt.TagSection;

public class CustomHandoverRecipeParser extends RecipeParser<CustomHandoverRecipe> {

	public static ParseStage HANDOVER = new ParseStage();
	private List<ItemStack> items = new ArrayList<>();
	private ItemStack base;

	private int damage;
	private int amount;
	private String addtag;
	private String removetag;

	public CustomHandoverRecipeParser(Plugin plugin, String recipename, String key) {
		super(plugin, recipename, key);
	}

	private boolean addShapeless(ItemStack item) {
		return items.size() < 10 ? items.add(item) : false;
	}

	@Override
	public void parseLine(String line) throws RecipeParseException {
		if(line.matches("I[ 1-9]= .*")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), ParseStage.INPUT);
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(addShapeless(item))throw new ItemStateException("InputItemError:");
			}
		}else if(line.toLowerCase().startsWith("B = ")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), HANDOVER);
			if(item.getAmount() != 1)throw new ItemAmountException(item.getAmount());
			this.base = item;
		}else if(line.toLowerCase().startsWith("+damage ")) {
			damage = parseInt(line.substring(8).trim());
		}else if(line.toLowerCase().startsWith("+amount ")) {
			amount = parseInt(line.substring(8).trim());
		}else if(line.toLowerCase().startsWith("+addtag ")) {
			addtag = new TagSection(line.substring(8).trim()).toString();
		}else if(line.toLowerCase().startsWith("+removetag ")) {
			addtag = new TagSection(line.substring(11).trim()).toString();
		}
	}

	private int parseInt(String str) throws NumberParseException {
		try {
			return Integer.parseInt(str);
		}catch(NumberFormatException e) {
			throw new NumberParseException(str);
		}
	}

	@Override
	public boolean isReady() {
		return base != null && !dorpped;
	}

	@Override
	public Collection<CustomHandoverRecipe> createRecipe() {
		super.buildProducts();
		return Lists.newArrayList(new CustomHandoverRecipe(getKey(), base, items, damage, amount, addtag, removetag, getOptions(), getCheck(), getProduct()));
	}

	@Override
	public String getCause() {
		return null;
	}
}
