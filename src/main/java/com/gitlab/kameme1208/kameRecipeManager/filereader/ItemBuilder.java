package com.gitlab.kameme1208.kameRecipeManager.filereader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemParseException.Type;
import com.gitlab.kameme1208.kameRecipeManager.inventory.CraftingItemStack;
import com.gitlab.kameme1208.kameRecipeManager.utils.Utils;

import kame.api.nbt.TagSection;

public abstract class ItemBuilder {

	public abstract ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) throws ItemParseException;
	private static Map<String, ItemBuilder> builders = new HashMap<>();

	static {
		builders.put("locname", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) {
				im.setLocalizedName(line);
				return im;
			}
		});
		builders.put("name", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) {
				im.setDisplayName(line);
				return im;
			}
		});
		builders.put("lore", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) {
				List<String> lore = im.getLore();
				if(lore == null)lore = new ArrayList<>();
				lore.add(line);
				im.setLore(lore);
				return im;
			}
		});
		builders.put("ench", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) throws ItemParseException {
				String[] enchs = line.split(":");
				if(enchs.length != 2)throw new ItemParseException(line, Type.ARGUMENT);
				@SuppressWarnings("deprecation")
				Enchantment e = enchs[0].matches("[0-9]+") ? e = Enchantment.getById(Utils.toInt(enchs[0], -1)) : Enchantment.getByName(enchs[0]);
				if(e == null)throw new ItemParseException(enchs[0], Type.ENCHANT);
				im.addEnchant(e, Utils.toInt(enchs[1], -1), true);
				return im;
			}
		});
		builders.put("flag", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) throws ItemParseException {
				try {
					im.addItemFlags(line.equalsIgnoreCase("ALL") ? ItemFlag.values() : new ItemFlag[] {ItemFlag.valueOf(line.toUpperCase())});
				}catch(Exception e) {
					throw new ItemParseException(line, Type.ITEMFLAG);
				}
				return im;
			}
		});
		builders.put("tag", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) throws ItemParseException {
				try {
					item.setItemMeta(im);
					addSection(new TagSection(item), new TagSection(line)).save();
				}catch(Exception e) {
					throw new ItemParseException(line, Type.NBT);
				}
				return item.getItemMeta();
			}
		});
		builders.put("setkeep", new ItemBuilder() {
			public ItemMeta execute(CraftingItemStack item, ItemMeta im, String line, ParseStage stage) throws ItemParseException {
				item.setKeep(true);
				return im;
			}

		});
	}

	private static Material parseMaterial(String name) throws ItemParseException {
		if(name.equalsIgnoreCase("AIR") || name.indexOf("minecraft:air") == 0)return Material.AIR;
		String item = name.substring(0, name.indexOf(':', name.startsWith("minecraft:") ? 10 : 0));
		@SuppressWarnings("deprecation")
		Material material = Bukkit.getUnsafe().getMaterialFromInternalName(name.toLowerCase());
		if(material.equals(Material.AIR))material = Material.matchMaterial(item.toUpperCase());
		if(material == null)throw new ItemParseException(item, Type.MATERIAL);
		return material;
	}

	public static MaterialData parseItem(String data) throws ItemParseException {
		Material mate = parseMaterial(data);
		short dura = Short.parseShort(data.substring(data.indexOf(':', data.startsWith("minecraft:") ? 11 : 1), data.length()));
		ItemStack item = new ItemStack(mate, 1, dura);
		return item.getData();
	}

	public static CraftingItemStack parseItemStack(String line, ParseStage stage) throws ItemParseException {
		String[] metas = line.trim().split(" @");
		StringBuilder builder = new StringBuilder(metas[0]);
		if(metas[0].startsWith("minecraft:"))builder.setCharAt(10, ';');
		String[] item = builder.toString().split(":", 3);
		Material material = parseMaterial(metas[0].replace(";", ":"));
		short dat = item.length > 1 ? (short) Utils.toInt(item[1], -1) : -1;
		int amount = item.length > 2 ? amount = Utils.toInt(item[2], 1) : 1;
		CraftingItemStack result = new CraftingItemStack(material, amount, dat == -1 ? 32767 : dat);
		ItemMeta im = result.getItemMeta();
		for(String meta : Arrays.asList(metas).subList(1, metas.length)) {
			item = meta.split(":", 2);
			ItemBuilder b = builders.get(item[0]);
			if(b == null)throw new ItemParseException(item[0] + " is not Suppoted.", Type.OTHER) {};
			b.execute(result, im, item[1], stage);
		}
		result.setItemMeta(im);
		return result;
	}

	private static TagSection addSection(TagSection base, TagSection write) {
		for(String tag : write.keySet()) {
			TagSection section = write.getSection(tag, false);
			if(section != null) {
				addSection(base.getSection(tag, true), section);
				continue;
			}
			List<TagSection> listsection = write.getSectionList(tag, false);
			if(listsection != null) {
				List<TagSection> listbase = base.getSectionList(tag, true);
				for(int i = 0; i < listsection.size(); i++) {
					TagSection sec = listsection.get(i);
					if(sec == null)continue;
					if(listbase.size() > i) {
						TagSection se = listbase.get(i);
						if(se == null) {
							listbase.set(i, sec);
						}else {
							addSection(se, sec);
						}
					}else {
						listbase.add(sec);
					}
				}
				continue;
			}
			base.set(tag, write.get(tag));
		}
		return base;
	}
}
