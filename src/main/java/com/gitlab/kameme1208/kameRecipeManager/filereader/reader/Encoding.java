package com.gitlab.kameme1208.kameRecipeManager.filereader.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.file.NoEncodeSuppotException;

public class Encoding {

	private static final List<Charset> encodings = new ArrayList<>();
	private static final Comparator<? super Map.Entry<Charset,Integer>> compare = (x, y) -> x.getValue() - y.getValue();
	static {
		for(String enc : new String[]{"Shift_JIS", "UTF-8", "UTF-16", "MS932", "EUC-JP"}) {
			try {
				encodings.add(Charset.forName(enc));
			}catch(Exception e) {}
		}
	}
	
	public static Charset getEncoding(ByteBuffer bytes) throws NoEncodeSuppotException {
		Map<Charset, Integer> point = new HashMap<>();
		for(Charset set : encodings) {
			try {
				int l = 0;
				for(char c : set.newDecoder().decode(bytes).array()) {
					l += ('0' <= c && c <='z') || ('あ' <= c && c <= 'ん') ? 1 : -1;
				}
				point.put(set, l);
			}catch(Exception e) {
				point.put(set, Integer.MIN_VALUE);
			}
		}
		Optional<Entry<Charset, Integer>> set = point.entrySet().stream().sorted(compare).findFirst();
		return set.orElseThrow(NoEncodeSuppotException::new).getKey();
	}
	
}
