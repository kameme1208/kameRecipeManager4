package com.gitlab.kameme1208.kameRecipeManager.filereader.reader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductAdd;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductReplace;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

import kame.kameplayer.Warnings;
import kame.kameplayer.Warnings.Warning;

public abstract class RecipeParser<T extends CustomRecipe> {
	private static final Map<String, Class<? extends RecipeParser<? extends CustomRecipe>>> REGISTRY = new HashMap<>();
	
	private static final RecipeParser<CustomRecipe> EMPTY = new RecipeParser<CustomRecipe>(null, "", "") {
		public void parseLine(String line) {}
		public boolean isReady() {return true;}
		public Collection<CustomRecipe> createRecipe() {return Collections.emptyList();}
		public String getCause() {return "";}
	};

	public static void register(String registername, Class<? extends RecipeParser<CustomRecipe>> clazz) {
		REGISTRY.put(registername, clazz);
	}

	public static RecipeParser<? extends CustomRecipe> createBuilder(String str, Plugin plugin, String craftname, String key) {
		try {
			return REGISTRY.get(str).getConstructor(Plugin.class, String.class, String.class).newInstance(plugin, craftname, key);
		}catch(Throwable e) {
			Warnings.addWarning(Main.getInstance(), new Warning(Main.getInstance(), "予期せぬ例外が発生しました。", e));
			return RecipeParser.empty();
		}
	}

	public static RecipeParser<? extends CustomRecipe> empty() {
		return EMPTY;
	}

	public static boolean isHeaderLine(String str) {
		return REGISTRY.containsKey(str.split(" ", 2)[0]);
	}

	public final String recipename;
	public final NamespacedKey key;

	public ItemStack result;

	public Options options;

	public Set<CheckMode> modes = new HashSet<>();

	public ProductReplace replace;
	public List<ProductAdd> add = new ArrayList<>();
	public ArrayList<ProductRecipe> products = new ArrayList<>();
	protected boolean dorpped;

	public RecipeParser(Plugin plugin, String recipename, String key) {
		this.recipename = recipename;
		this.key = new NamespacedKey(plugin, key);
	}

	public boolean setResult(ItemStack item) {
		if(result != null)return false;
		result = item;
		return true;
	}

	public void addOption(String line) {
		options.addOptions(line);
	}

	public void addCheckMode(CheckMode check) {
		if(check != null)modes.add(check);
	}

	public ItemStack getResult() {
		return result;
	}

	public String getName() {
		return recipename;
	}

	protected NamespacedKey getKey() {
		return key;
	}

	protected Set<CheckMode> getCheck() {
		return modes;
	}

	protected Options getOptions() {
		return options;
	}

	protected List<ProductRecipe> getProduct() {
		return products;
	}

	public abstract void parseLine(String line) throws RecipeParseException;

	protected final void parseDefaultLine(String line) throws RecipeParseException {
		if(line.startsWith("=== ")) {
			if(!setResult(ItemBuilder.parseItemStack(line.substring(4), ParseStage.RESULT)))throw new ItemStateException("ResultItemError:");
		}else if(line.toLowerCase().startsWith("$option @")) {
			for(String opt : line.substring(9).split(" @"))addOption(opt);
		}else if(line.toLowerCase().startsWith("$options @")) {
			for(String opt : line.substring(10).split(" @"))addOption(opt);
		}else if(line.toLowerCase().startsWith("$checkmode ")) {
			for(String ckm : line.substring(11).split(" "))addCheckMode(CheckMode.fromName(ckm));
		}else if(line.toLowerCase().startsWith("$nearblock ")) {
			String position = line.substring(11, line.indexOf(':', 12));
			if(!position.matches("-?[0-9]+,-?[0-9]+,-?[0-9]+"))throw new ItemStateException("IllegalPositionError:");
			ItemBuilder.parseItemStack(line.substring(line.indexOf(':', 11) + 1), ParseStage.OPTION);
			addOption(line.substring(1));
		}else if(line.matches("[+-=][0-9]+(\\.[0-9]+)?% = .*")) {
			if(line.charAt(0) == '+') {
				createAddProduct(line);
			}else {
				createReplaceProduct(line);
			}
		}else {
			parseLine(line);
		}
	}

	private void createReplaceProduct(String line) throws ItemParseException {
		int per = (int) (Double.parseDouble(line.substring(1, line.indexOf('%'))) * 1000);
		ItemStack item = ItemBuilder.parseItemStack(line.substring(line.indexOf(" = ") + 2), ParseStage.PRODUCT);
		if(replace == null)replace = new ProductReplace();
		replace.addPerItem(per, item);
	}

	private void createAddProduct(String line) throws ItemParseException {
		int per = (int) (Double.parseDouble(line.substring(1, line.indexOf('%'))) * 1000);
		ItemStack item = ItemBuilder.parseItemStack(line.substring(line.indexOf(" = ") + 2), ParseStage.PRODUCT);
		add.add(new ProductAdd(per, item));
	}

	protected void buildProducts() {
		if(replace != null) {
			products.add(replace);
			if(result == null) {
				result = new ItemStack(Material.WORKBENCH);
				ItemMeta meta = result.getItemMeta();
				meta.setDisplayName("§b§lChance Recipe §ks");
				meta.setLore(replace.getList());
				result.setItemMeta(meta);
			}
		}
		products.addAll(add);
	}
	public abstract boolean isReady();

	public abstract Collection<T> createRecipe();
	
	

	public abstract String getCause();
}
