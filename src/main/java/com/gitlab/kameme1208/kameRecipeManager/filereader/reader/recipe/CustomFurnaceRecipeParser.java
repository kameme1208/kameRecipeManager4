package com.gitlab.kameme1208.kameRecipeManager.filereader.reader.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.NumberParseException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeParser;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomFurnaceRecipe;
import com.google.common.collect.Lists;

public class CustomFurnaceRecipeParser extends RecipeParser<CustomFurnaceRecipe> {

	private ItemStack input;

	private int time = 200;
	private float exp = 0;

	private List<CraftingEffect> effects = new ArrayList<>();
	private List<CraftingSound>  sounds  = new ArrayList<>();


	public CustomFurnaceRecipeParser(Plugin plugin, String recipename, String key) {
		super(plugin, recipename, key);
	}

	protected boolean setInput(ItemStack item) {
		if(input != null)return false;
		input = item;
		return true;
	}

	@Override
	public void parseLine(String line) throws RecipeParseException {
		if(line.matches("I = .*")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), ParseStage.INPUT);
			if(setInput(item))throw new ItemStateException("InputItemError:");
		}else if(line.toLowerCase().startsWith("$time ")) {
			this.time = parseInt(line.substring(6).trim());
		}else if(line.toLowerCase().startsWith("$times ")) {
			this.time = parseInt(line.substring(7).trim());
		}else if(line.toLowerCase().startsWith("$exp ")) {
			this.exp = parseFroat(line.substring(5).trim());
		}else if(line.toLowerCase().startsWith("$effect ")) {
			String str[] = line.substring(8).trim().split(":", 2);
			effects.add(new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]));
		}else if(line.toLowerCase().startsWith("$sound ")) {
			String str[] = line.substring(7).trim().split(":", 2);
			sounds.add(new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]));
		}
	}

	private int parseInt(String str) throws NumberParseException {
		try {
			return Integer.parseInt(str);
		}catch(NumberFormatException e) {
			throw new NumberParseException(str);
		}
	}

	private float parseFroat(String str) throws NumberParseException {
		try {
			return Float.parseFloat(str);
		}catch(NumberFormatException e) {
			throw new NumberParseException(str);
		}
	}

	@Override
	public boolean isReady() {
		return input != null && getResult() != null && !dorpped;
	}

	@Override
	public Collection<CustomFurnaceRecipe> createRecipe() {
		super.buildProducts();
		return Lists.newArrayList(new CustomFurnaceRecipe(getKey(), input, getResult(), time, exp, effects, sounds, getOptions(), getCheck(), getProduct()));
	}

	@Override
	public String getCause() {
		return null;
	}

}
