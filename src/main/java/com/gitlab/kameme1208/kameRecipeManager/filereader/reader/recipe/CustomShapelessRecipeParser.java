package com.gitlab.kameme1208.kameRecipeManager.filereader.reader.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeParser;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapelessRecipe;
import com.google.common.collect.Lists;

public class CustomShapelessRecipeParser extends RecipeParser<CustomShapelessRecipe> {

	private List<ItemStack> items = new ArrayList<>();

	public CustomShapelessRecipeParser(Plugin plugin, String recipename, String key) {
		super(plugin, recipename, key);
	}

	private boolean addShapeless(ItemStack item) {
		return items.size() < 10 ? items.add(item) : false;
	}

	@Override
	public void parseLine(String line) throws RecipeParseException {
		if(line.matches("I[ 1-9]= .*")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), ParseStage.INPUT);
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(addShapeless(item))throw new ItemStateException("InputItemError:");
			}
		}
	}

	@Override
	public boolean isReady() {
		return items.size() != 0 && getResult() != null && !dorpped;
	}

	@Override
	public Collection<CustomShapelessRecipe> createRecipe() {
		super.buildProducts();
		return Lists.newArrayList(new CustomShapelessRecipe(getKey(), items, getResult(), getOptions(), getCheck(), getProduct()));
	}

	@Override
	public String getCause() {
		return null;
	}
}
