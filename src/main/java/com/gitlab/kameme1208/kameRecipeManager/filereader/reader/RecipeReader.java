package com.gitlab.kameme1208.kameRecipeManager.filereader.reader;

import java.util.List;
import java.util.logging.Level;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.recipe.Recipes;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class RecipeReader {

	public class Result {
		private long added;
		private long warn;
		private long line;
		private long cursol;
		private long fail;

		public long warnCount() {
			return warn;
		}

		public long addCount() {
			return added;
		}

		public long failCount() {
			return fail;
		}
	}

	private List<String> lines;
	private Result result = new Result();
	public RecipeReader(List<String> lines) {
		this.lines = lines;
		for(int i = 0; i < Math.min(1, lines.size()); i++) {
			if(lines.get(0).charAt(0) == '\uFFEF');
		}
	}

	public Result result() {
		RecipeParser<? extends CustomRecipe> parser = RecipeParser.empty();
		for(String line : lines) {
			if(RecipeParser.isHeaderLine(line)) {
				if(parser.isReady()) {
					String recipename = parser.recipename;
					parser.createRecipe().forEach(x -> Recipes.registerRecipe(recipename, x));
					result.added++;
				}else {
					FileRecipeReader.log.log(Level.SEVERE,"[ERROR] ROW:" + result.cursol + " " + parser.getCause());
					result.fail++;
				}
				String[] args = line.split(" ", 3);
				parser = RecipeParser.createBuilder(args[0], Main.getInstance(), args[1], args[2]);
				result.cursol = result.line;
			}else if(parser != null){
				try {
					parser.parseLine(line);
				}catch(RecipeParseException e) {
					FileRecipeReader.log.log(Level.SEVERE,"[ERROR] LINE:" + result.line + " " + e.getMessage());
					result.warn++;
				}
			}
			result.line++;
		}
		return result;
	}

}
