package com.gitlab.kameme1208.kameRecipeManager.filereader.reader.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemStateException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeParser;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomBigShapedRecipe;
import com.google.common.collect.Lists;

public class CustomBigShapedRecipeParser extends RecipeParser<CustomBigShapedRecipe> {

	private ArrayList<String> shapes = new ArrayList<>();
	private Map<Character, ItemStack> rawmap = new HashMap<>();

	public CustomBigShapedRecipeParser(Plugin plugin, String recipename, String key) {
		super(plugin, recipename, key);
	}

	private boolean addShape(String shape) {
		return shape.length() < 7 && shapes.size() < 7 ? shapes.add(shape) : false;
	}

	private boolean setItem(char key, ItemStack item) {
		return rawmap.containsKey(key) ? false : rawmap.put(key, item) == null;
	}

	@Override
	public void parseLine(String line) throws RecipeParseException {
		if(line.matches("[0-9a-zA-Z] = .*")) {
			ItemStack item = ItemBuilder.parseItemStack(line.substring(4), ParseStage.INPUT);
			if(setItem(line.charAt(0), item))throw new ItemStateException("InputItemError:");
		}else if(line.matches("([0-9a-zA-Z])( *\\+ *([0-9a-zA-Z])){6} *")) {
			if(!this.addShape(line.replaceAll("[^0-9a-zA-Z]", "")))throw new ItemStateException("InputItemError:");
		}
	}

	@Override
	public boolean isReady() {
		return rawmap.size() != 0 && shapes.size() != 0 && getResult() != null && !dorpped;
	}

	@Override
	public Collection<CustomBigShapedRecipe> createRecipe() {
		super.buildProducts();
		return Lists.newArrayList(new CustomBigShapedRecipe(getKey(), shapes.toArray(new String[0]), rawmap, getResult(), getOptions(), getCheck(), getProduct()));

	}

	@Override
	public String getCause() {
		if(shapes.size() == 0)return "Shape is not set! (0~9 + 0~9 + 0~9)";
		if(rawmap.size() == 0)return "Input item is empty! ([1-9] = Items)";
		if(getResult()== null)return "Output item is not set! (=== Items)";
		return "";
	}

}
