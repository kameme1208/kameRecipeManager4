package com.gitlab.kameme1208.kameRecipeManager.filereader;

public class ParseStage {
	public static final ParseStage RESULT = new ParseStage();
	public static final ParseStage INPUT = new ParseStage();
	public static final ParseStage OPTION = new ParseStage();
	public static final ParseStage PRODUCT = new ParseStage();
	public ParseStage() {}
}
