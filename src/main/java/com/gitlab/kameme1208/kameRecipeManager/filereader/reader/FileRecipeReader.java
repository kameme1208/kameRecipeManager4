package com.gitlab.kameme1208.kameRecipeManager.filereader.reader;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.file.NoEncodeSuppotException;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.RecipeReader.Result;
import com.gitlab.kameme1208.kameRecipeManager.recipe.Recipes;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaFurnaceRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapedRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.utils.Lang;
import com.google.common.collect.Lists;

import kame.kameplayer.Warnings;
import kame.kameplayer.Warnings.Warning;

public class FileRecipeReader {
	static final Logger log = Main.getInstance().getLogger();
	private static final List<Result> results = new ArrayList<>();
	private static long finecount;
	private static long failcount;
	public static final void readFile() {
		failcount = finecount = 0;
		File file = new File(Main.getInstance().getDataFolder(), "/recipes");
		try {
			Bukkit.resetRecipes();
			List<Recipe> def = Lists.newArrayList(Bukkit.recipeIterator());
			if(!file.isDirectory())file.mkdirs();
			long time = System.currentTimeMillis();
			log.log(Level.INFO, " ===============================================");
			File[] files = file.listFiles(x -> x.getName().endsWith(".txt") && !x.getName().startsWith("$"));
			if(files.length == 0) {
				log.info("Generate ExampleRecipe...");
				files = file.listFiles(x -> x.getName().endsWith(".txt") && !x.getName().startsWith("$"));
			}
			StringJoiner joiner = new StringJoiner(", ", "detected recipe " + files.length + "files [s] {", "}");
			for(File f : files)joiner.add(f.getName());
			log.log(Level.INFO, joiner.toString());
			for(File f : files)readFile(f);
			log.log(Level.INFO, "load recipe " + (float) (System.currentTimeMillis() - time) / 1000.0F + "[s]");
			log.log(Level.INFO, "Added the " + finecount + " recipe[s]");
			log.log(failcount > 0 ? Level.SEVERE : Level.INFO , "failed recipe is " + failcount);
			log.log(Level.INFO, " ===============================================");
			if(failcount > 0)Warnings.addWarning(Main.getInstance(), new Warning(Main.getInstance(), "追加に失敗したレシピが" + failcount + "個あります", null));
			loadDefault(def);
		}catch(RuntimeException t) {
			Warnings.addWarning(Main.getInstance(), new Warning(Main.getInstance(), "何らかの原因でファイルの読み込みに失敗しました", t));
		}
		results.clear();
	}
	
	private static void readFile(File file) {
		try {
			RecipeReader parse = new RecipeReader(getLine(file));
			Result result = parse.result();
			result.warnCount();
			results.add(result);
			if(result.warnCount() > 0)Warnings.addWarning(Main.getInstance(), new Warning(Main.getInstance(), file.getName() + " で一部正常でない部分が" + result.warnCount() + "個あります", null));
			log.log(result.warnCount() > 0 ? Level.WARNING : Level.INFO , "Complete loaded " + file.getName() + " " + result.addCount() + " Added " + result.warnCount() + " Warning");
		}catch(NoEncodeSuppotException e) {
			log.log(Level.SEVERE, file.getName() + " is not suppoted encode type");
			log.log(Level.SEVERE, file.getName() + " はこのプラグインでは開くことが出来ないエンコード形式です。");
		}catch(IOException e) {
			log.log(Level.SEVERE, file.getName() + " can not read file");
			log.log(Level.SEVERE, file.getName() + " の読み込みに失敗しました");
		}
	}
	
	private static List<String> getLine(File file) throws NoEncodeSuppotException, IOException {
		Path path = Paths.get(file.getPath());
		Charset c = Encoding.getEncoding(ByteBuffer.wrap(Files.readAllBytes(path)));
		String cn = c.toString();
		List<String> list = Files.readAllLines(path, c);
		String encode = cn.equals("UTF-8") && list.size() > 0 && list.get(0).length() > 0 && list.get(0).charAt(0) == '\uFEFF' ? cn + " BOM" : cn;
		log.log(Level.INFO, "Loading to " + file.getName() + "    EncodeType = " + encode);
		return list;
	}
	
	public static void loadDefault(List<Recipe> recipes) {
		for(Recipe r : recipes) {
			try {
				if(r instanceof ShapedRecipe) {
					Collection<ItemStack> items = ((ShapedRecipe) r).getIngredientMap().values();
					if((items.size() == 1 || items.size() == 9) && new HashSet<>(items).size() == 1) {
						ShapelessRecipe re = new ShapelessRecipe(((ShapedRecipe)r).getKey(), r.getResult());
						items.forEach(item -> re.addIngredient(item.getData()));
						Recipes.registerDefaultRecipe(Lang.crafttable(), new VanillaShapelessRecipe(re));
					}else {
						ShapedRecipe re = (ShapedRecipe)r;
						Map<Character, ItemStack> itemmap = re.getIngredientMap();
						Recipes.registerDefaultRecipe(Lang.crafttable(), new VanillaShapedRecipe((ShapedRecipe) r));
						int sizeX = ((ShapedRecipe)r).getShape()[0].length();
						int sizeY = ((ShapedRecipe)r).getShape().length;
						if(sizeX > 1) {
							for(int i = 0; i < sizeY; i++) {
								int a = sizeX * i;//0 2 4
								itemmap.put((char)('a' + a), itemmap.put((char)('a' + sizeX - 1 + a), itemmap.get((char)('a' + a))));
							}
							if(itemmap.equals(re.getIngredientMap()))continue;
							itemmap.entrySet().removeIf(x -> x.getValue() == null);
							ShapedRecipe recipe = new ShapedRecipe(re.getKey(), re.getResult());
							recipe.shape(re.getShape());
							itemmap.forEach((x, y) -> recipe.setIngredient(x, y.getData()));
							Recipes.registerDefaultRecipe(Lang.crafttable(), new VanillaShapedRecipe((recipe)));
						}
					}
				}else if(r instanceof ShapelessRecipe) {
					Recipes.registerDefaultRecipe(Lang.crafttable(), new VanillaShapelessRecipe((ShapelessRecipe) r));
				}else if(r instanceof FurnaceRecipe) {
					Recipes.registerDefaultRecipe(Lang.furnace(), new VanillaFurnaceRecipe((FurnaceRecipe) r));
				}else {
					
				}
			}catch(Exception e) {
				e.getStackTrace();
			}
		}
	}

}
