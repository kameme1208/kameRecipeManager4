# Example Recipes
#説明書
#
# ====================    レシピテーブルについて    ====================
# << レシピテーブルとは >>
# 作業台や、かまど、金床など KameRecipeManager を導入することで
# カスタムレシピが使えるようになったブロックをレシピテーブルとします
#
# << レシピテーブルの判定 >>
# アイテムの説明文の部分に@craftとついているアイテムがレシピテーブル対象です
#
# << レシピテーブルのネームタグについて >>
# レシピテーブルと判定されたアイテムはアイテムに付けられていた名前がネームタグとして表示されます
# 「@hide」と説明文につけることでネームタグ表示を無効にすることが出来ます
#
# ====================    レシピの基本形について    ====================
# << 各レシピの宣言方法 >>
# 
# ~~~~~~~~~~~~~~~~ 定型レシピ ~~~~~~~~~~~~~~~~
# レシピ型宣言 | CRAFT レシピテーブル名
#              |
#              | 0 1 0
# 形宣言       | 1 2 1
#              | 0 1 0
#              |
# 必要アイテム | 1 = IRON_INGOT
#              | 2 = REDSTONE
#              |
# 完成アイテム | === 4xIRON_INGOT @name:アイテム名 @lore:説明文 @lore:説明文
#              |
# その他       | $options @level:1~10 @levelcost:1
#              | $checkmode name lore ench flag other regname reglore
#              | 
#
# 
# ~~~~~~~~~~~~~~~~不定型レシピ~~~~~~~~~~~~~~~~
# レシピ型宣言 | COMBINE レシピテーブル名
#              |
# 必要アイテム | I2= SAND
#              | I = CRAY
#              |
# 完成アイテム | === INK_SACK:7
#              |
# その他       | $options @pathsound:BLOCK_SAND_PLACE:p=2
#              | 
#
# 
# ~~~~~~~~~~~~~~~~かまどレシピ~~~~~~~~~~~~~~~~
# レシピ型宣言 | SMELT レシピテーブル名
#              |
# 必要アイテム | I = SAND @name:アイテム名 @ench:2:1
#              |
# 完成アイテム | === 3xSAND @name:アイテム名 @ench:2:1
#              |
# 固有宣言     | $time 1000
#              | $effect EXPLOSIONA
# その他       | $options @pathsound:BLOCK_SAND_PLACE:p=2
#              | 
#
#
#
#=== 280 @name:名前 @lore:出来たやつのLore文
#1 = STONE
#
#CRAFT 名前付ければ特殊加工台
#1 + 4 + 1
#1 + 3 + 2
#1 + 3 + 2
#=== 278 @name:§b黒曜石つるはし @tag:{display:{Name:"",Lore:["","",""]},ench:[{id:1,lvl:0}]}
# シャープ入れてなくてもコメントできるけど念のため入れることおすすめ
#1 = 49
#4 = DIAMOND
#3 = 280 @name:名前 @lore:出来たやつのLore文
#
#
#COMBINE 名前付ければ特殊加工台
#I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10
#=== BARRIER @name:§bほねっこ食べないの...？ @lore:§aほんとに食べなくていいの？
#                     ※食べられるのは別プラグインです
#
#
#SMELT 名前付ければ特殊かまど
#I = OBSIDIAN
#=== 289 @name:黒曜石の粉末 @lore:加熱注意 @ench:35:1
#$times:4000
#$effect:TILE_BREAK:data=49:s=0:c=0:r=0.2
#
#
#FUSION 名前付ければ調合台
#I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10
#=== BONE @tag:{HideFlags:1} @name:§bほねっこ.v2 @lore:§aとてもおいしい(右クリックで試食) @lore:§a20/20 @ench:1:0
#$times:100
#
#
#SMASH 名前付ければｶﾝｶﾝ
#I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10
#=== BONE @tag:{HideFlags:1} @name:§bほねっこ.v2 @lore:§aとてもおいしい(右クリックで試食) @lore:§a20/20 @ench:1:0
#
#
 * オプションのリスト
 * @name:<name>
 * @lore:<lore>
 * @ench:<ench>:<power>
 * @tag:{name:...}
 * @flag:<flagname|ALL>
 フラグの種類:  <小文字でも可能>
 # HIDE_ATTRIBUTES
 # HIDE_DESTROYS
 # HIDE_ENCHANTS
 # HIDE_PLACED_ON
 # HIDE_POTION_EFFECTS
 # HIDE_UNBREAKABLE
 # ALL
#
#
 * チェックタイプのオプション
 name lore ench flag other regname reglore
 * uncheckname			:アイテム名を無視
 * unchecklore			:説明文を無視
 * uncheckenchant		:エンチャントを無視
 * uncheckitemflags		:アイテムフラグを無視
 * uncheckother			:その他を無視
 * patternname			:名前を正規表現で比較
 * patternlore			:説明文を正規表現で比較
 使用例 $checkmode:uncheckname

 * レシピ特有のオプション
 * $times:<tick>			:時間を指定				:かまど、醸造台
 * $exp:<exp>				:経験値					:かまど
 * $effect:<effecttype>:...	:クラフト時エフェクト	:かまど、醸造台、金床
 * $sound:<soundtype>:...	:クラフト時サウンド		:かまど、醸造台、金床
 * $spell:<spell>			:指定スペル				:金床
 * $cost:<cost>				:消費耐久値				:金床
 * $needitem:<item>			:指定アイテム			:金床


 * 実行時に組み立てられるオプション
 * pathsound					:成功時の音
 * failsound					:失敗時の音
 * playsound					:クラフト準備時の音
  @playsound:<SOUND_NAME>:...
  @playsound:WOLF_BARK:v=1:p=1 (v - Volume, p - Pitch)


 * patheffect					:成功時のエフェクト
 * faileffect					:失敗時のエフェクト
 * playeffect					:クラフト準備時のエフェクト
  @playeffect:<EFFECT_NAME>:...
  @playeffect:TILE_BREAK:d=1,0:s=1:c=10:v:0.2,0.5,0.1:per:10
  (d - Data(id,meta), s - Speed, c - Count, rgb - Color, v - Vector(Offset), r - range, y - AddLocation(Y), per - tickper)


 * pathexpand					:成功時の爆発
 * failexpand					:失敗時の爆発
  @failexpand:<amount>


 * pathplcmd					:成功時のプレイヤーコマンド
 * failplcmd					:失敗時のプレイヤーコマンド
 * pathclcmd					:成功時のコンソールコマンド
 * failclcmd					:失敗時のコンソールコマンド
 * pathinfo						:成功時のメッセージ
 * failinfo						:失敗時のメッセージ
 * pathbroadcast				:成功時のサーバーメッセージ
 * failbroadcast				:失敗時のサーバーメッセージ
 * broadcast					:クラフト準備時のサーバーメッセージ
 * info							:クラフト準備時のメッセージ
 * permission					:必要パーミッション
  @info:<message...|command...>


 * levelcost					:クラフト成功時のレベルコスト
 * healthcost					:クラフト成功時の体力コスト
 * foodcost						:クラフト成功時の空腹コスト
 * saturationcost				:クラフト成功時の腹持ちレベルコスト
 * faillevelcost				:クラフト失敗時のレベルコスト
 * failhealthcost				:クラフト失敗時の体力レベルコスト
 * failfoodcost					:クラフト失敗時の空腹レベルコスト
 * failsaturationcost			:クラフト失敗時の腹持ちレベルコスト
  @cost:<amount>


 * world						:クラフトできるワールド
 * biome						:クラフトできるバイオーム
  @world:<name>


 * score						:必要スコア									(小-大:任意)
  @score:Objective:Name:10-20 (or 10~20)


 * level						:必要レベル									(小-大:任意)
 * lightlevelsky				:クラフトできる明るさレベル(空から)			(小-大:任意)
 * lightlevelblock				:クラフトできる明るさレベル(ブロックから)	(小-大:任意)
 * lightlevel					:クラフトできる明るさレベル(どっちも)		(小-大:任意)
 * health						:クラフトできる体力							(小-大:任意)
 * hunger						:クラフトできる満腹度						(小-大:任意)
 * saturation					:クラフトできる隠れ満腹度					(小-大:任意)
 * time							:クラフトできる時間							(小-大:任意)
 * totaltime					;クラフトできるサーバートータル時間			(小-大:任意)
 * blockpower					:クラフトできる赤石入力						(小-大:任意)
  @level:<levelmin>
  @level:<min>~<max>
  @level:10-20 (or 10~20)


 * blocks						:周りにあるブロック
  @blocks:<FACE>:<TYPE>
  @blocks:0,-1,0:STONE
  @blocks:UP:DOAMOND_BLOCK

#
#今のところこれくらいだよ追加していく予定だからその時はよろしくね
#

↓ここから実際に少しだけレシピを追加してみる

/*
 * 圧縮台という作業台に不定形レシピでレシピを作成する
 * 8個の丸石を1個の圧縮丸石にすることができる
 */

COMBINE 圧縮台
I8= COBBLESTONE
=== COBBLESTONE @name:圧縮ブロック @lore:丸石x8 @ench:0:1 @flag:ALL

COMBINE 圧縮台
I = COBBLESTONE @name:圧縮ブロック @lore:丸石x8 @ench:0:1 @flag:ALL
=== COBBLESTONE:0:8

/*
 * さらに8個の圧縮丸石を1個の超圧縮丸石にすることができます
 */

COMBINE 圧縮台
I8= COBBLESTONE @name:圧縮ブロック @lore:丸石x8 @ench:0:1 @flag:ALL
=== COBBLESTONE @name:超圧縮ブロック @lore:丸石x64 @ench:0:1 @flag:ALL

COMBINE 圧縮台
I = COBBLESTONE @name:超圧縮ブロック @lore:丸石x64 @ench:0:1 @flag:ALL
=== COBBLESTONE:0:8 @name:圧縮ブロック @lore:丸石x8 @ench:0:1 @flag:ALL

/*
 * さらに8個の超圧縮丸石を1個の激圧縮丸石にすることができます
 * またこの圧縮にはレベル1を要求するオプションを付加します
 * また作成時に音、メッセージ、エフェクトをつけてみます
 * カラーコードは「§」セクションでできます
 */

COMBINE 圧縮台
I8= COBBLESTONE @name:超圧縮ブロック @lore:丸石x64 @ench:0:1 @flag:ALL
=== COBBLESTONE @name:激圧縮ブロック @lore:丸石x512 @ench:0:1 @flag:ALL
$option @level:1 @levelcost:1 @playsound:WOLF_BARK:p=2
$option @info:§b[クラフト] §aこのクラフトはレベルが1以上で可能です
$option @pathinfo:§b[クラフト] §a激圧縮ブロックを作成しました

※横に長くなりすぎるオプションは分けて書くことも可能です

COMBINE 圧縮台
I = COBBLESTONE @name:激圧縮ブロック @lore:丸石x512 @ench:0:1 @flag:ALL
=== COBBLESTONE:0:8 @name:超圧縮ブロック @lore:丸石x64 @ench:0:1 @flag:ALL



/*
 * 製錬レシピの追加 粉砕機というかまどに黒曜石を製錬(粉砕)するレシピを追加
 */

SMELT 粉砕機
※アイテム名にはBukkit:ID以外にもminecraft:id,数字IDも使用可能です
I = minecraft:obsidian
=== FIREWORK_CHARGE @name:§b黒曜石の粉 @lore:§4吸引注意 @ench:0:1 @flag:all

/*
 * 製錬レシピには燃焼時間、製錬時の音、製錬時のエフェクトなども指定が可能です
 */

SMELT 高温かまど
I = FIREWORK_CHARGE @name:§b黒曜石の粉 @lore:§4吸引注意 @ench:0:1 @flag:all
=== MAGMA_CREAM @name:§b溶けた黒曜石 @lore:§cドロドロ @ench:0:1 @flag:all
$times:1600
※80秒
$effect:EXPLOSION
$sound:BLOCK_WATER_AMBIENT:v=0.5:p=0.5

/*
 * 醸造台レシピはかまどと同様は燃焼時間、製錬時の音、製錬時のエフェクトなども指定が可能です
 * このクラフトでは同時に3つのアイテムまで要求することがでます
 * 鶏肉から錬金するレシピ　
 * またクラフトには確率分岐を確率追加があります(調合以外でも使用可能
 */

FUSION 調合台
I = STONE
I = COOKED_CHICKEN
I = STONE
=== GOLD_INGOT
-10% = GOLD_INGOT
-70% = STONE
//70%が石をドロップ、10%で金をドロップ、残り20%はクラフト失敗(アイテムが出ない)
$times:200


/*
 * 金が増えるか増えないかわからないレシピ
 * 最低1個　最高5個
 */

FUSION 調合台
I3= GOLD_INGOT
=== GOLD_INGOT
+50% = GOLD_INGOT
+50% = GOLD_INGOT
+50% = GOLD_INGOT
+50% = GOLD_INGOT
$times:200


/*
 * 叩きレシピでは斧と金床を使ったクラフト方法
 * クラフトで要求できるアイテムの数には制限はありません
 * また、スペルを指定することで、金床の文字入力欄に書いた文字が一致した場合のみクラフトが成功します
 * クラフトに使用する斧とクラフト時の耐久の消費量も設定できます
 * 耐久力のエンチャントをすることで消費を減らすこともできます
 * 計算式「消費量 = 指定数 - (0~1ランダム * 耐久レベル)　0以下になった場合は無視 耐久が10で最大10の消費が抑えられます
 */

※一度の指定では最大9個までです、それ以上要求する場合は圧縮レシピもしくは別の行に指定してください

SMASH 匠たん
I = TNT
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I8= COBBLESTONE
I = DIAMOND
=== MONSTER_EGG @tag:{EntityTag:{id:"Creeper"}}
$spell:匠よ来い
$cost:100


/*
 * 間違えているとその間違っている行と間違っている場所が表示されます
 * 行番号の見る事の出来るテキストエディタを使うことをお勧めします
 * このままでは300行目がそんなアイテムは存在しないと表示されるでしょう
 */

SMELT これ間違い
I = aaa:こんなID存在しないよん
I = STONE @name:かまどに2つの入力はありません
=== てすてす ここも間違えています


/*
 * オプションでの文字や、アイテムの名前、説明文は
 * 一部の文字を使うことでサーバー内の動的情報に置き換えることができます
 */
#
#かまどレシピ以外で使用可能
 * <player>			:プレイヤー名
 * <level>			:プレイヤーレベル
 * <UUID>			:プレイヤーのUUID
 * <health>			:体力
 * <foodlevel>		:満腹度
 * <saturation>		:隠れ満腹度(腹持ちみたいな数字があるの！
 * <playerX>		:プレイヤー座標 X
 * <playerY>		:プレイヤー座標 Y
 * <playerZ>		:プレイヤー座標 Z
 * <score_文字>		:プレイヤーのスコアの値
#
#全てのレシピで使用可能
 * <locationX>		:ブロックの座標 X
 * <locationY>		:ブロックの座標 Y
 * <locationZ>		:ブロックの座標 Z
 * <time>			:ワールド時間 (0-24000)
 * <fulltime>		:サーバー総時間
 * <skylevel>		:ブロックの空からの明るさレベル
 * <blocklevel>		:ブロックのブロックからの明るさレベル
 * <lightlevel>		:明るさレベル(上2つの大きい方)
 * <World>			:ワールド名
 * <biome>			:バイオーム(小文字)
 * <BIOME>			:バイオーム(大文字)
#


/*
 * またオプションでの文字、アイテムの名前、説明文内で
 * 特定の文字で囲うことで四則演算をさせることも可能です　上で使用した数字も代入可能です
 * ==NUM(str)[]    []内に式を書くことで文字として返される
 ///例 ==NUM(str)[(10/2) == 5]  -> trueと帰ってくる

 * ==NUM(int)[]    []内に式を書くことで整数型として返される(整数にならない場合はエラーを出力)
 ///例 ==NUM(int)[10/2]  -> 5と帰ってくる

 * ==NUM(dec)[]    []内に式を書くことで小数型として返される(数字にならない場合はエラーを出力)
 ///例 ==NUM(dec)[10/2]  -> 5.0と帰ってくる

 * 内部でJavaScriptを使用しているので変数など別の事も出来ると思います(やり方知りません
 * +、–、*、/、% が主に計算で使え == <= >= != < > が比較で使用できます
 * 他にもいろいろできると思うので「JavaScript 演算子」等で自力で調べてください(バグ未確認です)
 */
