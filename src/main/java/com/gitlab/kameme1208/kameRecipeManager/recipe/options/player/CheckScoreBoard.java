package com.gitlab.kameme1208.kameRecipeManager.recipe.options.player;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

import kame.kameplayer.baseutils.Utils;

public class CheckScoreBoard extends CraftOption implements CraftCheckOption {

	/*
	 * @score:Objective:player:x~y
	 * @score:Objective:player:x-y
	 */
	private static final Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
	private String obj;
	private String score;

	@Override
	public void init(String str) {

		String[] split = str.split(":");

		obj = split[0];
		score = split[1];
	}

	@Override
	public boolean result(Player player, Block block, String str) {
		Location loc = block.getLocation();
		int sc = board.getObjective(Utils.engine(obj, player, loc, player)).getScore(Utils.engine(score, player, loc, player)).getScore();
		return OptionUtils.in(str, sc);
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
