package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.item.ItemParseException.Type;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException.ErrorType;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;

public class FailSetBlock extends CraftOption implements CraftFailOption {

	private static final Pattern pattern = Pattern.compile("(-?[0-9])+,(-?[0-9]+),(-?[0-9]+)");
	private MaterialData data;
	private Object obj;
	@Override
	public void init(String str) throws ItemParseException {
		int index = str.indexOf(':');
		if(index == -1) {
			throw new ItemParseException(str, Type.ARGUMENT);
		}
		String location = str.substring(0, index);
		data = ItemBuilder.parseItem(str.substring(index + 1));
		Matcher match = pattern.matcher(location);
		if(match.matches()) {
			int x = Integer.parseInt(match.group(1));
			int y = Integer.parseInt(match.group(2));
			int z = Integer.parseInt(match.group(3));
			obj = new Vector(x, y, z);
		}else {
			obj = BlockFace.valueOf(location.toUpperCase());
		}
	}

	@Override
	public void result(Player player, Block block, String str) throws OptionParseException {
		int index = str.indexOf(':');
		if(index == -1)throw new OptionParseException(str, ErrorType.ARGUMENT);
		block = obj instanceof Vector ? block.getLocation().add((Vector)obj).getBlock() : block.getRelative((BlockFace)obj);
		BlockState state = block.getState();
		state.setData(data);
		state.update();
	}

	@Override
	public boolean isOnce() {
		return true;
	}
}
