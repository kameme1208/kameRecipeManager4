package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.EffectHandler;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.SoundHandler;

public class CustomFurnaceRecipe extends CustomRecipe implements EffectHandler, SoundHandler {

	protected FurnaceRecipe recipe;
	private final ItemStack input;
	private final List<CraftingEffect> effects;
	private final List<CraftingSound> sounds;
	protected int time;
	protected float exp;

	public CustomFurnaceRecipe(NamespacedKey key, ItemStack input, ItemStack result, int time, float exp, List<CraftingEffect> effects, List<CraftingSound> sounds, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, result, options, modes, products);
		this.recipe = new FurnaceRecipe(result, input.getData());
		this.input = input.clone();
		this.effects = Collections.unmodifiableList(effects);
		this.sounds = Collections.unmodifiableList(sounds);
		this.time= time;
		this.exp = exp;
	}

	@Override
	public FurnaceRecipe getBukkitRecipe() {
		return recipe;
	}
	
	@Override
	public String getTypeName() {
		return "SMELT";
	}

	public ItemStack getInput() {
		return input.clone();
	}

	public List<CraftingEffect> getEffects() {
		return effects;
	}

	public List<CraftingSound> getSounds() {
		return sounds;
	}
	
	public int getCookTime() {
		return time;
	}
	
	public float getExperience() {
		return exp;
	}
}
