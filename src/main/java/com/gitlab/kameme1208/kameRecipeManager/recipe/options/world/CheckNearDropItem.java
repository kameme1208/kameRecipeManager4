package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException.ErrorType;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.process.ItemProcessor;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckNearDropItem extends CraftOption implements CraftCheckOption {
	//$nearitem:5:material:anount:data @
	private int radius;
	private ItemStack item;
	@Override
	public void init(String str) throws OptionParseException {
		int index = str.indexOf(':');
		try {
			radius = Integer.parseInt(str.substring(0, index));
			item = ItemBuilder.parseItemStack(str.substring(index + 1), ParseStage.OPTION);
		}catch(NumberFormatException e) {
			throw new OptionParseException(str.substring(0, index), ErrorType.NUMBER);
		}catch(Exception e) {
			throw new OptionParseException(str.substring(index + 1), ErrorType.ITEM);
		}
	}

	@Override
	public boolean result(Player player, Block block, String str) throws OptionParseException {
		Location loc = block.getLocation().add(0.5, 0.5, 0.5);
		for(Entity entity : loc.getWorld().getNearbyEntities(loc, radius, radius, radius))if(entity instanceof Item) {
			if(ItemProcessor.matchItem(item, ((Item) entity).getItemStack(), null, player, loc))return true;
		}
		return false;
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
