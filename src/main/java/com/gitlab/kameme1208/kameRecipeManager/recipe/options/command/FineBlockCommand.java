package com.gitlab.kameme1208.kameRecipeManager.recipe.options.command;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;

import kame.kameplayer.baseutils.Utils;
import kame.kameplayer.baseutils.VersionSelecter;
import kame.kameplayer.baseutils.VersionSelecter.SenderType;

public class FineBlockCommand extends CraftOption implements CraftFineOption {

	@Override
	public void result(Player player, Block block, String str) {
		Bukkit.dispatchCommand(VersionSelecter.getSender(player, block, SenderType.BLOCK), Utils.engine(str, player, block.getLocation(), player));
	}

	@Override
	public boolean isOnce() {
		return false;
	}
}
