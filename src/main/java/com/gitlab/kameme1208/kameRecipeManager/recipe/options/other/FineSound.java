package com.gitlab.kameme1208.kameRecipeManager.recipe.options.other;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;

public class FineSound extends CraftOption implements CraftFineOption {

	@Override
	public void result(Player player, Block block, String str) {
		try {
			if(str.indexOf("minecraft:") == 0)str = str.substring(10);
			String[] args = str.split(":");
			new CraftingSound(args[0], Arrays.copyOfRange(args, 1, args.length)).playSound(player, block.getLocation().add(0.5, 0.5, 0.5));
		} catch (RecipeParseException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + str + "]");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessage());
		}
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
