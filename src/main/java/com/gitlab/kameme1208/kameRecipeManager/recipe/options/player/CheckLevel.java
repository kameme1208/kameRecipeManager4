package com.gitlab.kameme1208.kameRecipeManager.recipe.options.player;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckLevel extends CraftOption implements CraftCheckOption {

	@Override
	public boolean result(Player player, Block block, String str) {
		return OptionUtils.in(str, player.getLevel());
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
