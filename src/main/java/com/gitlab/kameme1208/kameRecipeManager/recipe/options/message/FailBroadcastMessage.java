package com.gitlab.kameme1208.kameRecipeManager.recipe.options.message;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;

public class FailBroadcastMessage extends CraftOption implements CraftFailOption {
	
	@Override
	public void result(Player player, Block block, String str) {
		Bukkit.broadcastMessage(str);
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
