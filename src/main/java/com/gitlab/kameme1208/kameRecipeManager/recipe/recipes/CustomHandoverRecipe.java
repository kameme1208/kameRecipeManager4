package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.utils.Utils;

import kame.api.nbt.TagSection;

public class CustomHandoverRecipe extends CustomShapelessRecipe {

	private int adddamage;
	private int addamount;
	private String addtag;
	private String removetag;

	public CustomHandoverRecipe(NamespacedKey key, ItemStack base, List<ItemStack> list, int damage, int amount, String addtag, String removetag, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, list, base, options, modes, products);
		
		this.adddamage = damage;
		this.addamount = amount;

		this.addtag = addtag;
		this.removetag = removetag;
	}

	@Override
	public ShapelessRecipe getBukkitRecipe() {
		return recipe;
	}

	@Override
	public ItemStack getResult() {
		ItemStack item = result.clone();
		if(adddamage != 0)item.setDurability((short)(item.getDurability() + adddamage));
		if(addamount != 0)item.setAmount(addamount);
		if(addtag != null)addSection(new TagSection(item), new TagSection(addtag)).save();
		if(addtag != null)removeSection(new TagSection(item), new TagSection(removetag)).save();
		return item;
	}
	
	private TagSection addSection(TagSection base, TagSection write) {
		for(String tag : write.keySet()) {
			TagSection section = write.getSection(tag, false);
			if(section != null) {
				addSection(base.getSection(tag, true), section);
				continue;
			}
			List<TagSection> listsection = write.getSectionList(tag, false);
			if(listsection != null) {
				List<TagSection> listbase = base.getSectionList(tag, true);
				for(int i = 0; i < listsection.size(); i++) {
					TagSection sec = listsection.get(i);
					if(sec == null)continue;
					if(listbase.size() > i) {
						TagSection se = listbase.get(i);
						if(se == null) {
							listbase.set(i, sec);
						}else {
							addSection(se, sec);
						}
					}else {
						listbase.add(sec);
					}
				}
				continue;
			}
			base.set(tag, write.get(tag));
		}
		return base;
	}

	private TagSection removeSection(TagSection base, TagSection write) {
		for(String tag : write.keySet()) {
			TagSection section = write.getSection(tag, false);
			if(section != null) {
				removeSection(base.getSection(tag, true), section);
				continue;
			}
			List<TagSection> listsection = write.getSectionList(tag, false);
			if(listsection != null) {
				List<TagSection> listbase = base.getSectionList(tag, true);
				for(int i = 0; i < listsection.size(); i++) {
					TagSection sec = listsection.get(i);
					if(sec == null)continue;
					if(listbase.size() > i) {
						TagSection se = listbase.get(i);
						if(se == null) {
							listbase.remove(i);
						}else {
							removeSection(se, sec);
						}
					}
				}
				continue;
			}
			base.remove(tag);
		}
		return base;
	}

	@Override
	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(getResult(), player, loc);
	}
	
	@Override
	public String getTypeName() {
		return "TAKEOVER";
	}

	public TagSection getSection() {
		return new TagSection(addtag);
	}
}
