package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckBiome extends CraftOption implements CraftCheckOption {

	@Override
	public boolean result(Player player, Block block, String str) {
		Biome biome = Biome.valueOf(str.toUpperCase());
		return biome.equals(block.getBiome());
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
