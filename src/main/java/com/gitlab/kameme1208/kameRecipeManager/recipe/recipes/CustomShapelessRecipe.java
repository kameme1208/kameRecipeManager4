package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class CustomShapelessRecipe extends CustomRecipe {

	protected ShapelessRecipe recipe;
	private final List<ItemStack> list;

	public CustomShapelessRecipe(NamespacedKey key, List<ItemStack> list, ItemStack result, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, result, options, modes, products);
		this.recipe = new ShapelessRecipe(key, result);
		
		List<Material> mate = new ArrayList<>();
		List<ItemStack> item = new ArrayList<>();
		
		list.forEach(x -> item.add(x.clone()));
		item.sort((x, y) -> x.toString().compareTo(y.toString()));
		item.forEach(x -> mate.add(x.getType()));
		item.forEach(x -> recipe.addIngredient(x.getData()));
		
		this.materials = Collections.unmodifiableList(mate);
		this.list = Collections.unmodifiableList(item);
	}

	@Override
	public ShapelessRecipe getBukkitRecipe() {
		return recipe;
	}
	
	@Override
	public String getTypeName() {
		return "COMBINE";
	}

	public List<ItemStack> getIngredientList() {
		return list;
	}

	public List<Material> getMaterialList() {
		return materials;
	}
}
