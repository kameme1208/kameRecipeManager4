package com.gitlab.kameme1208.kameRecipeManager.recipe.options.player;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;

public class FineLevelCost extends CraftOption implements CraftFineOption {

	@Override
	public void result(Player player, Block block, String str) {
		int level = Integer.parseInt(str);
		player.setLevel(player.getLevel() - level);
	}

	@Override
	public boolean isOnce() {
		return true;
	}

}
