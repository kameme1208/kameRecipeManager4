package com.gitlab.kameme1208.kameRecipeManager.recipe.options.message;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftRunOption;

public class PlayInfoMessage extends CraftOption implements CraftRunOption {

	@Override
	public void result(Player player, Block block, String str) {
		player.sendMessage(str);
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
