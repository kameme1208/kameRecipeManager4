package com.gitlab.kameme1208.kameRecipeManager.recipe.options.player;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckPermission extends CraftOption implements CraftCheckOption {

	@Override
	public boolean result(Player player, Block block, String str) {
    	str = str.replaceAll(" ", "");
		StringBuilder builder = new StringBuilder();
		char[] c = str.toCharArray();
		int before = 0;
		for(int after = 0; after < c.length; after++) {
			switch(c[after]) {
			case '(':
			case ')':
			case '|':
			case '&':
			case '^':
				String buffer = str.substring(before, after);
				if(buffer.isEmpty()) {
					builder.append(c[after]);
				}else {
					builder.append(hasPerm(player, buffer));
					builder.append(c[after]);
				}
				before = after + 1;
				break;
			default :
			}
		}
		builder.append(hasPerm(player, str.substring(before, str.length())));
		return OptionUtils.engine(builder.toString());
	}

	private String hasPerm(Player player, String permission) {
		if(permission.startsWith("!")) {
			return !player.hasPermission(permission.substring(1)) ? "o" : "x";
		}else {
			return player.hasPermission(permission) ? "o" : "x";
		}
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
