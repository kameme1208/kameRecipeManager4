package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces;

import java.util.List;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;

public interface SoundHandler {

	public List<CraftingSound> getSounds();
}
