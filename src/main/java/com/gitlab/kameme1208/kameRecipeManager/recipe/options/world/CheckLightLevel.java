package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckLightLevel extends CraftOption implements CraftCheckOption {
	private static final BlockFace[] FACES = {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
	@Override
	public boolean result(Player player, Block block, String str) {

		int max;
		int min;
		String[] split = str.split(":");
		if(split.length == 1) {
			max = min = Integer.parseInt(split[0]);
		}else {
			int i = Integer.parseInt(split[0]);
			int j = Integer.parseInt(split[1]);
			max = Math.max(i, j);
			min = Math.min(i, j);
		}

		int blight = block.getLightFromBlocks();
		for(BlockFace face :FACES)blight = Math.max(blight, block.getRelative(face).getLightFromBlocks() - 1);
		int slight = block.getRelative(BlockFace.UP).getLightFromSky();
		for(BlockFace face :FACES)slight = Math.max(slight, block.getRelative(face).getLightFromSky() - 1);
		int light = Math.max(blight, slight);
		return min <= light && light <= max;
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
