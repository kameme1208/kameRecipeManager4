package com.gitlab.kameme1208.kameRecipeManager.recipe.options.command;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;

import kame.kameplayer.baseutils.Utils;

public class FineServerCommand extends CraftOption implements CraftFineOption {

	@Override
	public void result(Player player, Block block, String str) {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), Utils.engine(str, player, block.getLocation(), player));
	}

	@Override
	public boolean isOnce() {
		return false;
	}
}
