package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException.ErrorType;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ParseStage;
import com.gitlab.kameme1208.kameRecipeManager.process.ItemProcessor;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftWorldCheckOption;

public class CheckNearBlockNBT extends CraftOption implements CraftWorldCheckOption {

	private static final Pattern pattern = Pattern.compile("(-?[0-9])+,(-?[0-9]+),(-?[0-9]+)");
	private CheckNbtBlock check;

	public void init(String str) throws RecipeParseException {
		int index = str.indexOf(':');
		if(index == -1) {
			throw new OptionParseException(str, ErrorType.ARGUMENT);
		}
		String location = str.substring(0, index);
		String blocktype = str.substring(index + 1);
		Matcher match = pattern.matcher(location);
		if(match.matches()) {
			int x = Integer.parseInt(match.group(1));
			int y = Integer.parseInt(match.group(2));
			int z = Integer.parseInt(match.group(3));
			check = new CheckNbtBlock(ItemBuilder.parseItemStack(blocktype, ParseStage.OPTION), x, y, z);
		}else {
			throw new OptionParseException(location, ErrorType.NUMBER);
		}
	}

	@Override
	public boolean result(Player player, Block block, String str, BlockFace face) {
		return check.containtsMaterial(block, face);
	}

	@Override
	public boolean isOnce() {
		return false;
	}

	public class CheckNbtBlock {

		private ItemStack item ;
		private int x;
		private int y;
		private int z;

		public CheckNbtBlock(ItemStack item, int x, int y, int z) {
			this.item = item;
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public boolean containtsMaterial(Block block, BlockFace face) {
			switch(face) {
			case NORTH:
				block = block.getRelative(x, y, z);
				break;
			case EAST:
				block = block.getRelative(z, y, x);
				break;
			case SOUTH:
				block = block.getRelative(-x, y, -z);
				break;
			case WEST:
				block = block.getRelative(-z, y, -x);
				break;
			default:
				return false;
			}
			
			ItemStack item = TableData.getItem(block, block.getState().getData().toItemStack(1));
			if(item.getType() == Material.AIR) {
				return item.getType() == this.item.getType();
			}
			return ItemProcessor.matchItem(this.item, item, null, null, block.getLocation());
		}
	}

}
