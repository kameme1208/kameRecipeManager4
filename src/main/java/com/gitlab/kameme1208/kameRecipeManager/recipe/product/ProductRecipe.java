package com.gitlab.kameme1208.kameRecipeManager.recipe.product;

import org.bukkit.inventory.ItemStack;

public interface ProductRecipe {

	public ItemStack buildItem();
}
