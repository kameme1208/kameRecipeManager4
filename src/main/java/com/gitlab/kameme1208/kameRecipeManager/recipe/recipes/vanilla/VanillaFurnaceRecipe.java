package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomFurnaceRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.VanillaRecipe;

public class VanillaFurnaceRecipe extends CustomFurnaceRecipe implements VanillaRecipe {
	private static final List<CraftingEffect> EFFECT =  Collections.unmodifiableList(new ArrayList<>());
	private static final List<CraftingSound> SOUND = Collections.unmodifiableList(new ArrayList<>());
	private static final Options OPTIONS = new Options();
	private static final Set<CheckMode> MODE = new HashSet<>();
	private static final List<ProductRecipe> RECIPES = new ArrayList<>();
	private static final Map<Material, Float> exps = new HashMap<>();

	static {
		exps.put(Material.SAND,         0.10f);
		exps.put(Material.COBBLESTONE,  0.10f);
		exps.put(Material.NETHERRACK,   0.10f);
		exps.put(Material.STAINED_CLAY, 0.10f);
		exps.put(Material.CHORUS_FRUIT, 0.10f);
		exps.put(Material.SPONGE,       0.15f);
		exps.put(Material.LOG,          0.15f);
		exps.put(Material.LOG_2,        0.15f);
		exps.put(Material.CACTUS,       0.20f);
		exps.put(Material.NETHER_STALK, 0.20f);
		exps.put(Material.LAPIS_ORE,    0.20f);
		exps.put(Material.CLAY_BALL,    0.30f);
		exps.put(Material.MUTTON,       0.35f);
		exps.put(Material.PORK,         0.35f);
		exps.put(Material.POTATO,       0.35f);
		exps.put(Material.RAW_BEEF,     0.35f);
		exps.put(Material.RAW_CHICKEN,  0.35f);
		exps.put(Material.RAW_FISH,     0.35f);
		exps.put(Material.IRON_ORE,     0.70f);
		exps.put(Material.REDSTONE_ORE, 0.70f);
		exps.put(Material.GOLD_ORE,     1.00f);
		exps.put(Material.DIAMOND_ORE,  1.00f);
		exps.put(Material.EMERALD_ORE,  1.00f);
	}

	public VanillaFurnaceRecipe(FurnaceRecipe recipe) {
		super(NamespacedKey.minecraft(""), recipe.getResult(), recipe.getInput(), 80, exps.getOrDefault(recipe.getResult().getType(), 0f), EFFECT, SOUND, OPTIONS, MODE, RECIPES);
		super.recipe = recipe;
	}
}
