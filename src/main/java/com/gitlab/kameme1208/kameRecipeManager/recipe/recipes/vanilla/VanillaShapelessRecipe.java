package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.inventory.ShapelessRecipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.VanillaRecipe;

public class VanillaShapelessRecipe extends CustomShapelessRecipe implements VanillaRecipe {
	private static final Options OPTIONS = new Options();
	private static final Set<CheckMode> MODE = new HashSet<>();
	private static final List<ProductRecipe> RECIPES = new ArrayList<>();

	public VanillaShapelessRecipe(ShapelessRecipe recipe) {
		super(recipe.getKey(), recipe.getIngredientList(), recipe.getResult(), OPTIONS, MODE, RECIPES);
		super.recipe = recipe;
	}

}
