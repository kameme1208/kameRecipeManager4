package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.EffectHandler;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.SoundHandler;

public class CustomFusionRecipe extends CustomRecipe implements EffectHandler, SoundHandler {

	private final List<ItemStack> items;
	private final List<CraftingEffect> effects;
	private final List<CraftingSound> sounds;
	private final int time;
	private final float exp;

	public CustomFusionRecipe(NamespacedKey key, List<ItemStack> list, ItemStack result, int time, float exp, List<CraftingEffect> effects, List<CraftingSound> sounds, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, result, options, modes, products);
		
		List<Material> mate = new ArrayList<>();
		List<ItemStack> item = new ArrayList<>();
		
		list.forEach(x -> item.add(x.clone()));
		item.sort((x, y) -> x.toString().compareTo(y.toString()));
		item.forEach(x -> mate.add(x.getType()));
		
		super.materials = Collections.unmodifiableList(mate);
		this.items = Collections.unmodifiableList(item);
		this.effects = Collections.unmodifiableList(effects);
		this.sounds = Collections.unmodifiableList(sounds);
		
		this.time = time;
		this.exp = exp;
	}

	@Override
	public Recipe getBukkitRecipe() {
		return null;
	}
	
	@Override
	public String getTypeName() {
		return "FUSION";
	}

	@Override
	public List<CraftingSound> getSounds() {
		return sounds;
	}

	@Override
	public List<CraftingEffect> getEffects() {
		return effects;
	}

	public List<ItemStack> getIngredientList() {
		return items;
	}
	
	public int getCookTime() {
		return time;
	}
	
	public float getExperience() {
		return exp;
	}
}
