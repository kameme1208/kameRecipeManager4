package com.gitlab.kameme1208.kameRecipeManager.recipe.options.command;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;

import kame.kameplayer.baseutils.Utils;

public class FinePlayerCommand extends CraftOption implements CraftFineOption {

	@Override
	public void result(Player player, Block block, String str) {
		Location loc = block.getLocation();
		if(player.isOp()) {
			player.performCommand(Utils.engine(str, player, loc, player));
		}else {
			player.setOp(true);
			player.performCommand(Utils.engine(str, player, loc, player));
			player.setOp(false);
		}
	}

	@Override
	public boolean isOnce() {
		return false;
	}
}
