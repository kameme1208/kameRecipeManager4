package com.gitlab.kameme1208.kameRecipeManager.recipe.options.other;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;

public class FailExpand extends CraftOption implements CraftFailOption {

	private float power;

	@Override
	public void init(String str) {
		power = Float.parseFloat(str);
	}

	@Override
	public void result(Player player, Block block, String str) {
		Location loc = block.getLocation().add(0.5, 0.5, 0.5);
		loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), Math.abs(power), power < 0, power < 0);
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
