package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.block.Furnace;
import org.bukkit.inventory.ItemStack;

import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.gitlab.kameme1208.kameRecipeManager.process.ItemProcessor;
import com.gitlab.kameme1208.kameRecipeManager.process.smelt.CustomFurnace;

public class FuelRecipe {

	private final ItemStack fuel;

	private Set<String> furnacenames = new HashSet<>();
	private final int burntime;
	private final double cookrate;

	public FuelRecipe(ItemStack fuel, int burntime, double cookrate) {
		this.fuel = fuel;
		this.burntime = burntime;
		this.cookrate = cookrate;
	}

	public boolean isMatchFurnace(Furnace furnace) {
		if(!ItemProcessor.matchItem(fuel, furnace.getInventory().getFuel(), null, null, furnace.getLocation()))return false;
		return TableData.fromBlock(furnace.getBlock()).removeAll(furnacenames);
	}

	public void setBurnTime(CustomFurnace furnace) {
		try {
			Furnace fur = furnace.getFurnace();
			furnace.setCookRate(cookrate);
			Method getTileEntity = fur.getClass().getMethod("getTileEntity");
			Object tileEntityFurnace = getTileEntity.invoke(fur);
			Method setPropaty = tileEntityFurnace.getClass().getMethod("setProperty", int.class, int.class);
			setPropaty.invoke(tileEntityFurnace, 1, burntime);
			setPropaty.invoke(tileEntityFurnace, 0, burntime);
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}
}
