package com.gitlab.kameme1208.kameRecipeManager.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;

import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class Recipes {

	private static class Table {
		private String tablename;
		private final Collection<Material> materials;
		private final Class<? extends CustomRecipe> type;
		
		private Table(String tablename, CustomRecipe recipe) {
			this.tablename = tablename;
			this.materials = recipe.getPattern();
			this.type = recipe.getClass();
		}
		
		private Table(String tablename, Collection<Material> materials, Class<? extends CustomRecipe> type) {
			this.tablename = tablename;
			this.materials = materials;
			this.type = type;
		}
		
		@Override
		public int hashCode() {
			return type.hashCode() + materials.hashCode() + tablename.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			if(this == o)return true;
			if(o instanceof Table) {
				Table t = (Table)o;
				return type.equals(t.type) && materials.equals(t.materials) && tablename.equals(t.tablename);
			}else {
				return false;
			}
		}
	}
	
	private static final Map<Table, List<CustomRecipe>> RECIPE = new HashMap<>();
	private static final Map<Class<? extends CustomRecipe>, String> NAMES = new HashMap<>();

	public static void registerRecipe(String recipename, CustomRecipe recipe) {
		if(recipe.getBukkitRecipe() != null && !recipe.getResult().getType().equals(Material.AIR))Bukkit.addRecipe(recipe.getBukkitRecipe());
		registerDefaultRecipe(recipename, recipe);
	}
	
	public static void registerDefaultRecipe(String recipename, CustomRecipe recipe) {
		NAMES.put(recipe.getClass(), recipe.getTypeName());
		List<CustomRecipe> recipes = RECIPE.get(new Table(recipename, recipe));
		if(recipes == null)RECIPE.put(new Table(recipename, recipe), recipes = new ArrayList<>());
		recipes.add(recipe);
	}

	public static <T extends CustomRecipe> List<T> findRecipe(Class<T> type, Collection<String> names, Collection<Material> materials) {
		List<CustomRecipe> recipes = new ArrayList<>();
		for(String str : names)recipes.addAll(RECIPE.getOrDefault(new Table(str, materials, type), Collections.emptyList()));
		@SuppressWarnings("unchecked") List<T> ret = (List<T>)recipes;
		return ret;
	}

	public static Map<Class<? extends CustomRecipe>, TreeMap<String, List<CustomRecipe>>> getSortedAllRecipes() {
		Map<Class<? extends CustomRecipe>, TreeMap<String, List<CustomRecipe>>> map = new HashMap<>();
		for(Entry<Table, List<CustomRecipe>> entry : RECIPE.entrySet()) {
			Table table = entry.getKey();
			List<CustomRecipe> recipe = entry.getValue();
			
			TreeMap<String, List<CustomRecipe>> tree = map.get(table.type);
			if(tree == null)map.put(table.type, tree = new TreeMap<>());
			
			List<CustomRecipe> recipes = tree.get(table.tablename);
			if(recipes == null)tree.put(table.tablename, recipes = new ArrayList<>());
			recipes.addAll(recipe);
		}
		return map;
	}
	
	public static String gerTypeNameFromClass(Class<? extends CustomRecipe> clazz) {
		return NAMES.get(clazz);
	}

	public static void initialize() {
		Bukkit.resetRecipes();
		RECIPE.clear();
	}
	
}
