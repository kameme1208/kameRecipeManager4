package com.gitlab.kameme1208.kameRecipeManager.recipe.options;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftRunOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftWorldCheckOption;

public class Options {

	List<CraftCheckOption> checks = new ArrayList<>();
	List<CraftWorldCheckOption> worldchecks = new ArrayList<>();
	List<CraftFineOption> fines = new ArrayList<>();
	List<CraftFailOption> fails = new ArrayList<>();
	List<CraftRunOption> runs = new ArrayList<>();

	private boolean once;

	public void addOptions(String option) {
		CraftOption opt = CraftOption.createOption(option);
		if(opt == null) {
			return;
		}if(opt instanceof CraftCheckOption) {
			checks.add((CraftCheckOption) opt);
		}if(opt instanceof CraftWorldCheckOption) {
			worldchecks.add((CraftWorldCheckOption) opt);
		}if(opt instanceof CraftFineOption) {
			fines.add((CraftFineOption) opt);
		}if(opt instanceof CraftFailOption) {
			fails.add((CraftFailOption) opt);
		}if(opt instanceof CraftRunOption) {
			runs.add((CraftRunOption) opt);
		}
		if(opt.isOnce())once = true;
	}

	public boolean isOnce() {
		return once;
	}
}
