package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class CustomBigShapedRecipe extends CustomRecipe {

	private final String[] shapes;
	private final Map<Character, ItemStack> map;
	private final Map<Character, Material> materials = new HashMap<>();
	
	public CustomBigShapedRecipe(NamespacedKey key, String[] shapes, Map<Character, ItemStack> map, ItemStack result, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, result, options, modes, products);
		map.forEach((x, y) -> materials.put(x, y.getType()));
		Map<Character, ItemStack> maps = new HashMap<>();
		this.shapes = shapes;
		String str = String.join("", shapes).replace(" ", "");
		super.materials = new ArrayList<>();
		for(int i = 0; i < str.length(); i++)if(materials.containsKey(str.charAt(i))) {
			maps.put(str.charAt(i), map.get(str.charAt(i)));
			super.materials.add(materials.get(str.charAt(i)));
		}
		this.map = Collections.unmodifiableMap(maps);
	}

	@Override
	public Recipe getBukkitRecipe() {
		return null;
	}
	
	@Override
	public String getTypeName() {
		return "BIGCRAFT";
	}
	
	public Map<Character, ItemStack> getIngredientMap() {
		return map;
	}

	public Map<Character, Material> getMaterialMap() {
		return materials;
	}

	public String[] getShape() {
		return shapes.clone();
	}
}
