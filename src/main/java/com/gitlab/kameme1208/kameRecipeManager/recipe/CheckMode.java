package com.gitlab.kameme1208.kameRecipeManager.recipe;

public enum CheckMode {
	uncheckname,
	unchecklore,
	uncheckenchant,
	uncheckitemflags,
	uncheckother,
	patternname,
	patternlore,
	patternnbt,
	containsnbt
	;

	public static CheckMode fromName(String ckm) {
		try {
			return CheckMode.valueOf(ckm.toLowerCase());
		}catch(IllegalArgumentException ew) {
			return null;
		}
	}
}
