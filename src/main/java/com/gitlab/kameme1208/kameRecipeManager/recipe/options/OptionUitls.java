package com.gitlab.kameme1208.kameRecipeManager.recipe.options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class OptionUitls {

	private static final ItemStack fail = new ItemStack(Material.BARRIER);

	static {
		ItemMeta im = fail.getItemMeta();
		im.setDisplayName(ChatColor.AQUA + "resultFail");
		im.setLore(Arrays.asList(ChatColor.RED + "クラフトに失敗しました"));
		fail.setItemMeta(im);
	}

	private static HashMap<String, List<OptionUitls>> options = new HashMap<>();

	private CustomRecipe recipe;
	private Block block;
	private Result result;

	private OptionUitls(Player player, CustomRecipe recipe, Location loc, Result result) {
		this.recipe = recipe;
		this.block = loc.getBlock();
		this.result = result;
	}

	private boolean playResult(Player player) {
		return recipe.getOption().checks.stream().allMatch(x -> CraftOption.result(x, player, block, x.toString())) &&(
				recipe.getOption().worldchecks.stream().allMatch(x -> CraftOption.result(x, player, block, x.toString(), BlockFace.NORTH))||
				recipe.getOption().worldchecks.stream().allMatch(x -> CraftOption.result(x, player, block, x.toString(), BlockFace.SOUTH))||
				recipe.getOption().worldchecks.stream().allMatch(x -> CraftOption.result(x, player, block, x.toString(), BlockFace.EAST))||
				recipe.getOption().worldchecks.stream().allMatch(x -> CraftOption.result(x, player, block, x.toString(), BlockFace.WEST)));
	}

	private void playPath(Player player) {
		recipe.getOption().fines.forEach(x -> CraftOption.result(x, player, block, x.toString()));
	}

	private void playFail(Player player) {
		recipe.getOption().fails.forEach(x -> CraftOption.result(x, player, block, x.toString()));
	}

	public static void clear(HumanEntity player) {
		options.remove(player.getUniqueId().toString());
	}

	public static void clear(Location loc) {
		options.remove(loc.toString());
	}

	public static void buildCraft(Player player, CustomRecipe recipe, Location loc, Result result) {
		List<OptionUitls> buffer = options.getOrDefault(player.getUniqueId().toString(), new ArrayList<>());
		buffer.add(new OptionUitls(player, recipe, loc, result));
		options.put(player.getUniqueId().toString(), buffer);
	}

	public static void build(Player player, CustomRecipe recipe, Location loc) {
		List<OptionUitls> buffer = options.getOrDefault(loc.toString(), new ArrayList<>());
		buffer.add(new OptionUitls(player, recipe, loc, null));
		options.put(loc.toString(), buffer);
	}

	public static HashMap<UUID, Buffer> buffers = new HashMap<>();
	private static class Buffer extends BukkitRunnable {
		private final Player player;
		private OptionUitls option;
		public void run() {
			option.recipe.getOption().runs.forEach(x -> CraftOption.result(x, player, option.block, x.toString()));
			option.recipe.getOption().runs.clear();
		}

		private Buffer(Player player) {
			this.player = player;
			this.runTask(Main.getInstance());
		}

		private void updateResult(OptionUitls option) {
			this.option = option;
			buffers.put(player.getUniqueId(), this);
		}

	}

	public static ItemStack prepareCraft(Player player) {
		List<OptionUitls> buffer = options.getOrDefault(player.getUniqueId().toString(), new ArrayList<>());
		if(buffer.size() == 0)return null;
		if(buffer.size() == 1) {
			Buffer buf = buffers.get(player.getUniqueId());
			if(buf == null)buf = new Buffer(player);
			buf.updateResult(buffer.get(0));
			return buffer.get(0).recipe.getResult();
		}
		for(OptionUitls option : buffer)if(option.playResult(player)) {
			Buffer buf = buffers.get(player.getUniqueId());
			if(buf == null)buf = new Buffer(player);
			buf.updateResult(option);
			return option.recipe.getResult();
		}
		return fail;
	}

	public static <T extends CustomRecipe> T prepare(Player player, Location loc, Class<T> clazz) {
		List<OptionUitls> buffer = options.getOrDefault(loc.toString(), new ArrayList<>());
		for(OptionUitls option : buffer)if(option.playResult(player)) {
			option.playPath(player);
			return clazz.cast(option.getRecipe());
		}
		for(OptionUitls option : buffer) {
			option.playFail(player);
			break;
		}
		return null;
	}

	public static OptionUitls crafting(Player player) {
		List<OptionUitls> buffer = options.getOrDefault(player.getUniqueId().toString(), new ArrayList<>());
		for(OptionUitls option : buffer) {
			if(option.playResult(player))return option;
		}
		for(OptionUitls option : buffer) {
			option.playFail(player);
			return null;
		}
		return null;
	}

	public static void resultCraft(Player player, OptionUitls option) {
		option.playPath(player);
	}

	public static boolean isOneByOne(Player player) {
		for(OptionUitls utils : options.getOrDefault(player.getUniqueId().toString(), new ArrayList<>())) {
			if(utils.recipe.getOption().isOnce()) {
				return true;
			}
		}
		return false;
	}
	public CustomRecipe getRecipe() {
		return recipe;
	}

	public Location getLocation() {
		return block.getLocation().add(0.5, 0.5, 0.5);
	}

	public Result getResult() {
		return result;
	}

	public class Result {

	}
}
