package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.option.OptionParseException.ErrorType;
import com.gitlab.kameme1208.kameRecipeManager.filereader.ItemBuilder;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftWorldCheckOption;

public class CheckNearBlock extends CraftOption implements CraftWorldCheckOption {

	private static final Pattern pattern = Pattern.compile("(-?[0-9])+,(-?[0-9]+),(-?[0-9]+)");
	private CheckBlock check;

	public void init(String str) throws RecipeParseException {
		int index = str.indexOf(':');
		if(index == -1) {
			throw new OptionParseException(str, ErrorType.ARGUMENT);
		}
		String location = str.substring(0, index);
		String blocktype = str.substring(index + 1);
		Matcher match = pattern.matcher(location);
		if(match.matches()) {
			int x = Integer.parseInt(match.group(1));
			int y = Integer.parseInt(match.group(2));
			int z = Integer.parseInt(match.group(3));
			check = new CheckBlock(x, y, z);
		}else {
			BlockFace face = BlockFace.valueOf(location.toUpperCase());
			check = new CheckBlock(face);
		}

		for(String matestr : blocktype.split("\\|")) {
			if(matestr.startsWith("!")) {
				MaterialData material = ItemBuilder.parseItem(matestr.substring(1));
				if(material != null)check.addBudMaterial(material);
			}else {
				MaterialData material = ItemBuilder.parseItem(matestr);
				if(material != null)check.addMaterial(material);
			}
		}
	}

	@Override
	public boolean result(Player player, Block block, String str, BlockFace face) {
		return check.containtsMaterial(block, face);
	}

	@Override
	public boolean isOnce() {
		return false;
	}


public class CheckBlock {
	private Set<MaterialData> blockmaterial = new HashSet<>();
	private Set<MaterialData> Blockmaterial = new HashSet<>();
	private int x;
	private int y;
	private int z;

	public CheckBlock(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public CheckBlock(BlockFace face) {
		this.x = face.getModX();
		this.y = face.getModY();
		this.z = face.getModZ();
	}

	public void addBudMaterial(MaterialData material) {
		Blockmaterial.add(material);
	}

	public void addMaterial(MaterialData material) {
		blockmaterial.add(material);
	}

	public boolean containtsMaterial(Block block, BlockFace face) {
		MaterialData type = null;
		switch(face) {
		case NORTH:
			type = block.getRelative(x, y, z).getState().getData();
			break;
		case EAST:
			type = block.getRelative(z, y, x).getState().getData();
			break;
		case SOUTH:
			type = block.getRelative(-x, y, -z).getState().getData();
			break;
		case WEST:
			type = block.getRelative(-z, y, -x).getState().getData();
			break;
		default:
			return false;
		}
		type.equals(null);
		return match(blockmaterial, type) && nonmatch(Blockmaterial, type);
	}

	@SuppressWarnings("deprecation")
	private boolean match(Collection<MaterialData> base, MaterialData input) {
		Material mate = input.getItemType();
		byte inputdata = input.getData();
		for(MaterialData basemate : base) {
			byte data = basemate.getData();
			if(data == -1) {
				if(basemate.getItemType() == mate)return true;
			}else {
				if(basemate.getItemType() == mate && inputdata == data)return true;
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	private boolean nonmatch(Collection<MaterialData> base, MaterialData input) {
		Material mate = input.getItemType();
		byte inputdata = input.getData();
		for(MaterialData basemate : base) {
			byte data = basemate.getData();
			if(data == -1) {
				if(basemate.getItemType() == mate)return false;
			}else {
				if(basemate.getItemType() == mate && inputdata == data)return false;
			}
		}
		return true;
	}

}

}
