package com.gitlab.kameme1208.kameRecipeManager.recipe.options.other;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftingEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftRunOption;

public class PlayEffect extends CraftOption implements CraftRunOption {

	@Override
	public void result(Player player, Block block, String str) {
		try {
			if(str.indexOf("minecraft:") == 0)str = str.substring(10);
			String[] args = str.split(":");
			new CraftingEffect(args[0], Arrays.copyOfRange(args, 1, args.length)).playEffect(player, block.getLocation().add(0.5, 0.5, 0.5));
		} catch (RecipeParseException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + str + "]");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessage());
		}
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
