package com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface CraftCheckOption {

	public boolean result(Player player, Block block, String str) throws Throwable;

}
