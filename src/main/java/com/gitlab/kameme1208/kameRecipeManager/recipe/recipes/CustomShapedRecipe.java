package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;

public class CustomShapedRecipe extends CustomRecipe {

	protected ShapedRecipe recipe;
	private final Map<Character, ItemStack> map;
	private final Map<Character, Material> material;

	public CustomShapedRecipe(NamespacedKey key, String[] shapes, Map<Character, ItemStack> map, ItemStack result, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		super(key, result, options, modes, products);
		this.recipe = new ShapedRecipe(key, result.clone());
		
		this.recipe.shape(shapes);
		List<Material> mate = new ArrayList<>(); 
		
		Map<Character, Material> items = new HashMap<>();
		map.entrySet().removeIf(x -> x.getValue() == null);
		map.forEach((x, y) -> items.put(x, y.getType()));
		for(char c : String.join("", shapes).replace(" ", "").toCharArray())mate.add(items.get(c));
		this.materials = Collections.unmodifiableList(mate);
		this.material = Collections.unmodifiableMap(items);
		this.map = Collections.unmodifiableMap(new HashMap<>(map));
	}

	@Override
	public ShapedRecipe getBukkitRecipe() {
		return recipe;
	}
	
	@Override
	public String getTypeName() {
		return "CRAFT";
	}

	public Map<Character, ItemStack> getIngredientMap() {
		return map;
	}

	public Map<Character, Material> getMaterialMap() {
		return material;
	}

	public String[] getShape() {
		return recipe.getShape();
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof CustomShapedRecipe && super.equals(o) && ((CustomShapedRecipe)o).map.equals(map);
	}
}
