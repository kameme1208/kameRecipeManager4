package com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.Options;
import com.gitlab.kameme1208.kameRecipeManager.recipe.product.ProductRecipe;
import com.gitlab.kameme1208.kameRecipeManager.utils.Utils;

public abstract class CustomRecipe {

	protected final NamespacedKey key;
	protected final ItemStack result;
	protected final Options options;
	protected final Set<CheckMode> modes;
	protected final List<ProductRecipe> products;
	protected List<Material> materials;
	
	protected CustomRecipe(NamespacedKey key, ItemStack result, Options options, Set<CheckMode> modes, List<ProductRecipe> products) {
		this.key = key;
		this.result = result.clone();
		this.modes = Collections.unmodifiableSet(modes);
		this.products = Collections.unmodifiableList(products);
		this.options = options;
	}
	
	public NamespacedKey getKey() {
		return key;
	}

	public ItemStack getResult() {
		return result.clone();
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(getResult(), player, loc);
	}

	public Options getOption() {
		return options;
	}

	public Set<CheckMode> getCheckMode() {
		return modes;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

	public Collection<Material> getPattern(){
		return materials;
	}

	public abstract Recipe getBukkitRecipe();

	public abstract String getTypeName();
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof CustomRecipe) {
			CustomRecipe ob = (CustomRecipe)o;
			return key == ob.key || (key != null && key.equals(ob.key)) &&
					result.equals(ob.result) && modes.equals(ob.modes)
					&& products.equals(ob.products) && options.equals(ob.options);
		}
		return false;
	}
}