package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckLightLevelSky extends CraftOption implements CraftCheckOption {

	private static final BlockFace[] FACES = {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};

	@Override
	public boolean result(Player player, Block block, String str) {
		int light = block.getLightFromSky();
		for(BlockFace face :FACES)light = Math.max(light, block.getRelative(face).getLightFromSky() - 1);
		return OptionUtils.in(str, light);
	}

	@Override
	public boolean isOnce() {
		return false;
	}
}
