package com.gitlab.kameme1208.kameRecipeManager.recipe.options.world;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;

public class CheckBlockPower extends CraftOption implements CraftCheckOption {

	@Override
	public boolean result(Player player, Block block, String str) {
		return OptionUtils.in(str, block.getBlockPower());
	}

	@Override
	public boolean isOnce() {
		return false;
	}

}
