package com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public interface CraftWorldCheckOption {

	public boolean result(Player player, Block block, String str, BlockFace face) throws Throwable;

}
