package com.gitlab.kameme1208.kameRecipeManager.recipe.options.command;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.recipe.options.CraftOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;

import kame.kameplayer.baseutils.Utils;
import kame.kameplayer.baseutils.VersionSelecter;
import kame.kameplayer.baseutils.VersionSelecter.SenderType;

public class FailExcuteCommand extends CraftOption implements CraftFailOption {

	@Override
	public void result(Player player, Block block, String str) {
		Bukkit.dispatchCommand(VersionSelecter.getSender(player, block, SenderType.EXECUTE), Utils.engine(str, player, block.getLocation(), player));
	}

	@Override
	public boolean isOnce() {
		return false;
	}
}
