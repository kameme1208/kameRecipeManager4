package com.gitlab.kameme1208.kameRecipeManager.recipe.options;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FailBlockCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FailExcuteCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FailPlayerCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FineBlockCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FineExcuteCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FinePlayerCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.command.FineServerCommand;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftCheckOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFailOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftFineOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftRunOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.interfaces.CraftWorldCheckOption;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.FailBroadcastMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.FailInfoMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.FineBroadcastMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.FineInfoMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.PlayBroadcastMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.message.PlayInfoMessage;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.FailEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.FailSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.FineEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.FineSound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.PlayEffect;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.other.PlaySound;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckFoodLevel;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckHealth;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckLevel;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckPermission;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckSaturation;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.CheckScoreBoard;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FailFoodCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FailHealthCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FailLevelCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FailSaturationCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FineFoodCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FineHealthCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FineLevelCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.player.FineSaturationCost;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckBiome;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckBlockPower;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckLightLevel;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckLightLevelBlock;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckLightLevelSky;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckNearBlock;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckNearBlockNBT;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckNearDropItem;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckTime;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckTotalTime;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.CheckWorld;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.FailSetBlock;
import com.gitlab.kameme1208.kameRecipeManager.recipe.options.world.FineSetBlock;

import kame.kameplayer.Warnings;
import kame.kameplayer.Warnings.Warning;

public abstract class CraftOption {

	static final Map<String, Class<? extends CraftOption>> REGISTRY = new HashMap<>();

	private String str;

	public abstract boolean isOnce();

	static {

		/*  Sounds  */
		CraftOption.registerOption("pathsound",         FineSound.class);
		CraftOption.registerOption("failsound",         FailSound.class);
		CraftOption.registerOption("playsound",         PlaySound.class);
		/*  Effects  */
		CraftOption.registerOption("patheffect",        FineEffect.class);
		CraftOption.registerOption("faileffect",        FailEffect.class);
		CraftOption.registerOption("playeffect",        PlayEffect.class);
		/*  Explodes  */
		CraftOption.registerOption("pathexpand",        PlaySound.class);
		CraftOption.registerOption("failexpand",        PlaySound.class);
		/*  Commands  */
		CraftOption.registerOption("pathblcmd",         FineBlockCommand.class);
		CraftOption.registerOption("failblcmd",         FailBlockCommand.class);
		CraftOption.registerOption("pathexcmd",         FineExcuteCommand.class);
		CraftOption.registerOption("failexcmd",         FailExcuteCommand.class);
		CraftOption.registerOption("pathplcmd",         FinePlayerCommand.class);
		CraftOption.registerOption("failplcmd",         FailPlayerCommand.class);
		CraftOption.registerOption("pathclcmd",         FineServerCommand.class);
		/*  ServerMessages  */
		CraftOption.registerOption("pathbloadcast",     FineBroadcastMessage.class);
		CraftOption.registerOption("failbloacdast",     FailBroadcastMessage.class);
		CraftOption.registerOption("playbloadcast",     PlayBroadcastMessage.class);
		/*  Messages  */
		CraftOption.registerOption("pathinfo",          FineInfoMessage.class);
		CraftOption.registerOption("failinfo",          FailInfoMessage.class);
		CraftOption.registerOption("playinfo",          PlayInfoMessage.class);
		/*  PlayerStates  */
		CraftOption.registerOption("foodlevel",         CheckFoodLevel.class);
		CraftOption.registerOption("saturation",        CheckSaturation.class);
		CraftOption.registerOption("health",            CheckHealth.class);
		CraftOption.registerOption("level",             CheckLevel.class);
		CraftOption.registerOption("permission",        CheckPermission.class);
		CraftOption.registerOption("score",             CheckScoreBoard.class);
		CraftOption.registerOption("foodcost",          FineFoodCost.class);
		CraftOption.registerOption("saturationcost",    FineSaturationCost.class);
		CraftOption.registerOption("healthcost",        FineHealthCost.class);
		CraftOption.registerOption("levelcost",         FineLevelCost.class);
		CraftOption.registerOption("failfoodcost",      FailFoodCost.class);
		CraftOption.registerOption("failsaturationcost",FailSaturationCost.class);
		CraftOption.registerOption("failhealthcost",    FailHealthCost.class);
		CraftOption.registerOption("faillevelcost",     FailLevelCost.class);
		/*  WorldStates  */
		CraftOption.registerOption("biome",             CheckBiome.class);
		CraftOption.registerOption("blockpower",        CheckBlockPower.class);
		CraftOption.registerOption("lightlevel",        CheckLightLevel.class);
		CraftOption.registerOption("lightlevelblock",   CheckLightLevelSky.class);
		CraftOption.registerOption("lightlevelblock",   CheckLightLevelBlock.class);
		CraftOption.registerOption("blocks",            CheckNearBlock.class);
		CraftOption.registerOption("nearblock",         CheckNearBlockNBT.class);
		CraftOption.registerOption("nearitem",          CheckNearDropItem.class);
		CraftOption.registerOption("time",              CheckTime.class);
		CraftOption.registerOption("totaltime",         CheckTotalTime.class);
		CraftOption.registerOption("world",             CheckWorld.class);
		CraftOption.registerOption("pathsetblock",      FineSetBlock.class);
		CraftOption.registerOption("failsetblock",      FailSetBlock.class);
	}

	@Override
	public String toString() {
		return str;
	}

	public void init(String str) throws RecipeParseException {}

	public static void registerOption(String optionName, Class<? extends CraftOption> clazz) {
		if(clazz.equals(CraftOption.class))
			throw new IllegalArgumentException("Do not register \"CraftOption.class\". It will loop!!");
		REGISTRY.put(optionName, clazz);
	}

	public static CraftOption createOption(String option) {
		if(Main.debug)Main.cast("RunOption = " + option);
		int index = option.indexOf(':');
		if(index == -1) {
			Exception e = new Exception("<kameRecipeManager> ERROR: OptionArgumentsError text=\"" + option + "\"");
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: 不正な型", e));
			return null;
		}
		String type = option.substring(0, index);
		Class<? extends CraftOption> clazz = REGISTRY.get(type);
		if(clazz == null) {
			Exception e = new Exception("<kameRecipeManager> ERROR: OptionNotFoundError text=\"" + type + "\"");
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: 未登録のオプション名", e));
			return null;
		}
		try {
			CraftOption opt = clazz.newInstance();
			opt.str = option.substring(index + 1);
			opt.init(opt.str);
			return opt;
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: 初期化エラー", e));
			return null;
		}
	}

	public static boolean result(CraftCheckOption option, Player player, Block block, String str) {
		try {
			return option.result(player, block, str);
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: ", e));
			return false;
		}
	}

	public static boolean result(CraftWorldCheckOption option, Player player, Block block, String str, BlockFace face) {
		try {
			return option.result(player, block, str, face);
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: ", e));
			return false;
		}
	}

	public static void result(CraftFineOption option, Player player, Block block, String str) {
		try {
			option.result(player, block, str);
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: ", e));
		}
	}

	public static void result(CraftFailOption option, Player player, Block block, String str) {
		try {
			option.result(player, block, str);
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: ", e));
		}
	}

	public static void result(CraftRunOption option, Player player, Block block, String str) {
		try {
			option.result(player, block, str);
		}catch(Throwable e) {
			Warnings.sendOperatorWarnings(new Warning(Main.getInstance(), "オプション実行時にエラーが発生しました: ", e));
		}
	}

	protected static class MinMax <T extends Number> {
		public Number min;
		public Number max;
		public MinMax(String numbers, T def) {
			String[] str = numbers.split("[-~/]");
			if(def == null) {
				throw new NullPointerException();
			}else if(Byte.class.isInstance(def)) {
				byte i = Byte.parseByte(str[0]);
				byte j = str.length > 1 ? Byte.parseByte(str[1]) : Byte.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else if(Short.class.isInstance(def)) {
				short i = Short.parseShort(str[0]);
				short j = str.length > 1 ? Short.parseShort(str[1]) : Short.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else if(Integer.class.isInstance(def)) {
				int i = Integer.parseInt(str[0]);
				int j = str.length > 1 ? Integer.parseInt(str[1]) : Integer.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else if(Long.class.isInstance(def)) {
				long i = Long.parseLong(str[0]);
				long j = str.length > 1 ? Long.parseLong(str[1]) : Long.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else if(Float.class.isInstance(def)) {
				float i = Float.parseFloat(str[0]);
				float j = str.length > 1 ? Float.parseFloat(str[1]) : Float.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else if(Double.class.isInstance(def)) {
				double i = Double.parseDouble(str[0]);
				double j = str.length > 1 ? Double.parseDouble(str[1]) : Double.MAX_VALUE;
				if(i>j){max=i;min=j;}else{max=j;min=i;}
			}else {
				throw new IllegalArgumentException();
			}
		}
	}

	protected static class OptionUtils {

		public static boolean in(byte min, byte check, byte max)  {
			return min <= check && check <= max;
		}

		public static boolean in(short min, short check, short max) {
			return min <= check && check <= max;
		}

		public static boolean in(int min, int check, int max) {
			return min <= check && check <= max;
		}

		public static boolean in(long min, long check, long max)  {
			return min <= check && check <= max;
		}

		public static boolean in(float min, float check, float max) {
			return min <= check && check <= max;
		}

		public static boolean in(double min, double check, double max) {
			return min <= check && check <= max;
		}

		public static boolean in(String numbers, byte base) {
			String[] str = numbers.split("[-~/]");
			byte i = Byte.parseByte(str[0]);
			byte j = str.length > 1 ? Byte.parseByte(str[1]) : Byte.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean in(String numbers, short base) {
			String[] str = numbers.split("[-~/]");
			short i = Short.parseShort(str[0]);
			short j = str.length > 1 ? Short.parseShort(str[1]) : Short.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean in(String numbers, int base) {
			String[] str = numbers.split("[-~/]");
			int i = Integer.parseInt(str[0]);
			int j = str.length > 1 ? Integer.parseInt(str[1]) : Integer.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean in(String numbers, long base) {
			String[] str = numbers.split("[-~/]");
			long i = Long.parseLong(str[0]);
			long j = str.length > 1 ? Long.parseLong(str[1]) : Long.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean in(String numbers, float base) {
			String[] str = numbers.split("[-~/]");
			float i = Float.parseFloat(str[0]);
			float j = str.length > 1 ? Float.parseFloat(str[1]) : Float.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean in(String numbers, double base) {
			String[] str = numbers.split("[-~/]");
			double i = Double.parseDouble(str[0]);
			double j = str.length > 1 ? Double.parseDouble(str[1]) : Double.MAX_VALUE;
			return i < j ? in(i, base, j) : in(j, base, i);
		}

		public static boolean engine(String line) {
			final Deque<Character> stack = new ArrayDeque<Character>();
			final Deque<Boolean> bool = new ArrayDeque<Boolean>();
			StringBuilder builder = new StringBuilder("(").append(line).append(")");
			for(int i; (i = builder.indexOf(" ")) != -1;)builder.deleteCharAt(i);
			for(int i; (i = builder.indexOf("^^")) != -1;)builder.deleteCharAt(i);
			for(int i; (i = builder.indexOf("||")) != -1;)builder.deleteCharAt(i);
			for(int i; (i = builder.indexOf("&&")) != -1;)builder.deleteCharAt(i);
			for(int i; (i = builder.indexOf("true")) != -1;)builder.replace(i, i + 4, "o");
			for(int i; (i = builder.indexOf("false")) != -1;)builder.replace(i, i + 5, "x");
			for(char c : builder.toString().toCharArray()) {
				if(c == 'o' || c == 'x') {
					bool.push(c == 'o');
				}else {
					while (!stack.isEmpty() && rank(stack.peek()) >= rank(c) && stack.peek() != '(') {
						boolean a = bool.pop();
						boolean b = bool.pop();
						switch(stack.pop()) {
						case '&': bool.push(a && b); break;
						case '|': bool.push(a || b); break;
						case '^': bool.push(a ^  b); break;
						}
					}
					if(c == ')') stack.pop();
					else stack.push(c);
				}
			}
			return bool.pop();
		}

		private static byte rank(char c) {
			switch(c) {
			case '(': return 2;
			case '&': return 1;
			case '|': return 1;
			case '^': return 1;
			case ')': return 0;
			}
			return -1;
		}
	}





}
