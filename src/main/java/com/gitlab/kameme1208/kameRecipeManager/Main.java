package com.gitlab.kameme1208.kameRecipeManager;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.gitlab.kameme1208.kameRecipeManager.command.RecipeCommand;
import com.gitlab.kameme1208.kameRecipeManager.filereader.reader.FileRecipeReader;
import com.gitlab.kameme1208.kameRecipeManager.process.crafting.CraftingListener;
import com.gitlab.kameme1208.kameRecipeManager.recipe.CheckMode;

import kame.kameplayer.Warnings;
import kame.kameplayer.Warnings.Warning;

public class Main extends JavaPlugin implements Listener {

	private static Main instance;
	public static boolean debug;


	public Main() {
		instance = this;
	}

	public static FileConfiguration config;
	private static boolean isEnableDrive;
	private static double offset;
	private static List<CheckMode> mode;

	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, this);

		Bukkit.getPluginManager().registerEvents(new TableData(), this);
		Bukkit.getPluginManager().registerEvents(new CraftingListener(), this);
		loadConfig();
		FileRecipeReader.readFile();
		RecipeCommand r = new RecipeCommand();
		getCommand("kamerecipe").setExecutor(r);
		getCommand("kamerecipe").setTabCompleter(r);
	}

	public static Main getInstance() {
		return instance;
	}

	@Override
	public void onDisable() {

	}

	@EventHandler
	public void onEnable(PluginEnableEvent event) {
		if(event.getPlugin() != this)return;
	}

	@EventHandler
	public void onDisable(PluginDisableEvent event) {
		if(event.getPlugin() != this)return;
	}

	public static void loadConfig() {
		instance.saveDefaultConfig();
		try {
			File file = new File(instance.getDataFolder(), "config.yml");
			config = YamlConfiguration.loadConfiguration(file);
			if(config.getDouble("configversion") != 3.3) {
				instance.getLogger().severe("不一致なバージョンのConfigを検知したので置き換えました");
				Warnings.addWarning(new Warning(instance, "不一致なバージョンのConfigを検知したので置き換えました", null));
			}
			isEnableDrive = config.getBoolean("GoogleDrive.enable");
			debug = config.getBoolean("debug");
			for(String str : config.getStringList("debaultcheckmode")) {
				CheckMode m = CheckMode.fromName(str.toLowerCase());
				if(m != null)mode.add(m);
			}
		} catch (Exception e) {
			Warnings.addWarning(new Warning(instance, "[kames.] Config error! Configを確認してください！", e));
		}
	}

	public static void cast(Object list) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
