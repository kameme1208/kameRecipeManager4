package com.gitlab.kameme1208.kameRecipeManager.process.metadata;

import org.bukkit.block.Block;

public class BlockMetadataValue extends KameRecipeMetadataValue {
	
	private Block block;
	
	public BlockMetadataValue(Block block) {
		super();
		this.block = block;
	}

	@Override
	public Block value() {
		return block;
	}

}
