package com.gitlab.kameme1208.kameRecipeManager.process.smelt;

import org.bukkit.block.Furnace;

public class CustomFurnace {

	private final Furnace furnace;
	private double cookrate;

	public CustomFurnace(Furnace furnace) {
		this.furnace = furnace;
	}

	public Furnace getFurnace() {
		return furnace;
	}

	public void setCookRate(double cookrate) {
		this.cookrate = cookrate;
	}
}
