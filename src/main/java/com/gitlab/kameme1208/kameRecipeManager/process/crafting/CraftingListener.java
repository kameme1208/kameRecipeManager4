package com.gitlab.kameme1208.kameRecipeManager.process.crafting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.gitlab.kameme1208.kameRecipeManager.process.InventorySifter;
import com.gitlab.kameme1208.kameRecipeManager.process.ItemProcessor;
import com.gitlab.kameme1208.kameRecipeManager.process.metadata.BlockMetadataValue;
import com.gitlab.kameme1208.kameRecipeManager.process.metadata.DisplayMetadataValue;
import com.gitlab.kameme1208.kameRecipeManager.recipe.Recipes;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapedRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.CustomShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.interfaces.CustomRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapedRecipe;
import com.gitlab.kameme1208.kameRecipeManager.recipe.recipes.vanilla.VanillaShapelessRecipe;
import com.gitlab.kameme1208.kameRecipeManager.utils.Lang;
import com.gitlab.kameme1208.kameRecipeManager.utils.display.WorkbenchDisplay;

public class CraftingListener implements Listener {
	
	@EventHandler
	private void onInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Set<String> include = TableData.fromBlock(event.getClickedBlock());
			if(!include.isEmpty()) {
				event.getPlayer().openInventory(Bukkit.createInventory(event.getPlayer(), InventoryType.WORKBENCH, include.iterator().next()));
				Block block = event.getClickedBlock();
				event.getPlayer().setMetadata("ViewTable", new BlockMetadataValue(block));
				event.getPlayer().setMetadata("ViewTable", new DisplayMetadataValue(new WorkbenchDisplay(block, event.getPlayer())));
				event.setCancelled(true);
			}else if(event.getClickedBlock().getType() == Material.WORKBENCH) {
				Block block = event.getClickedBlock();
				event.getPlayer().setMetadata("ViewTable", new BlockMetadataValue(block));
				event.getPlayer().setMetadata("ViewTable", new DisplayMetadataValue(new WorkbenchDisplay(block, event.getPlayer())));
			}
		}
	
	}
	
	@EventHandler
	private void onClose(InventoryCloseEvent event) {
		event.getPlayer().getMetadata("ViewTable").stream()
		.filter(DisplayMetadataValue.class::isInstance)
		.map(DisplayMetadataValue.class::cast)
		.forEach(x -> x.value().hideScore(100));
		event.getPlayer().removeMetadata("ViewTable", Main.getInstance());
	}
	
	@EventHandler
	private void onPreProcess(PrepareItemCraftEvent event) {
		if(event.isRepair()) {
			
		}else {
			CraftingInventory inv = event.getInventory();
			inv.setResult(null);
			Optional<Block> block = event.getView().getPlayer().getMetadata("ViewTable").stream()
					.filter(BlockMetadataValue.class::isInstance)
					.map(BlockMetadataValue.class::cast)
					.map(BlockMetadataValue::value)
					.findFirst();
			
			if(block.isPresent()) {
				Set<String> include = TableData.fromBlock(block.get());
				if(include.isEmpty())include.add(Lang.crafttable());
				Player player = (Player)event.getView().getPlayer();
				LinkedList<CustomRecipe> recipes = findFromMatrix(include, inv, player);
				if(!recipes.isEmpty()) {
					inv.setResult(recipes.poll().getResult((Player)event.getView().getPlayer(), block.get().getLocation()));
				}
				event.getView().getPlayer();
				
				
			}else {
			}
		}
	}
	
	public static LinkedList<CustomRecipe> findFromMatrix(Set<String> including, CraftingInventory inv, Player player) {
		inv.setResult(null);
		ItemStack[] matrix = inv.getMatrix();
		List<String> shapes = new ArrayList<>();
		Map<Character, ItemStack> items = InventorySifter.startSift(matrix, shapes);
		List<Material> materials = new ArrayList<>();
		for(char c : String.join("", shapes).toCharArray())materials.add(items.containsKey(c) ? items.get(c).getType() : null);
		if(including.isEmpty())including.add(Lang.crafttable());
		
		LinkedList<CustomRecipe> recipes = new LinkedList<>();
		
		//定型レシピの検索
		List<CustomShapedRecipe> shaped = Recipes.findRecipe(CustomShapedRecipe.class, including, materials);
		shaped.addAll(Recipes.findRecipe(VanillaShapedRecipe.class, including, materials));
		for(CustomShapedRecipe recipe : shaped)if(shapedFilter(recipe, shapes, items, player, inv.getLocation()))recipes.add(recipe);
		
		materials.removeIf(x -> x == null);
		materials.sort((x, y) -> x.toString().compareTo(y.toString()));
		//不定型レシピの検索
		List<CustomShapelessRecipe> shapeless = Recipes.findRecipe(CustomShapelessRecipe.class, including, materials);
		shapeless.addAll(Recipes.findRecipe(VanillaShapelessRecipe.class, including, materials));
		for(CustomShapelessRecipe recipe : shapeless)if(shapelessFilter(recipe, items.values(), player, inv.getLocation()))recipes.add(recipe);
		
		return recipes;
	}
	
	private static boolean shapelessFilter(CustomShapelessRecipe shapeless, Collection<ItemStack> list, Player player, Location loc) {
		return ItemProcessor.matchItem(shapeless.getIngredientList(), list, shapeless.getCheckMode(), player, loc);
	}

	private static boolean shapedFilter(CustomShapedRecipe base, List<String> shapes, Map<Character, ItemStack> map, Player player, Location loc) {
		if(!Arrays.asList(base.getShape()).equals(shapes))return false;
		return ItemProcessor.matchItem(base.getIngredientMap(), map, base.getShape(), Collections.emptySet(), player, loc);
	}
	
}
