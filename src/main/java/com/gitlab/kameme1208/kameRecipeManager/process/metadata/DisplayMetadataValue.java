package com.gitlab.kameme1208.kameRecipeManager.process.metadata;

import com.gitlab.kameme1208.kameRecipeManager.utils.display.Display;

public class DisplayMetadataValue extends KameRecipeMetadataValue {

	private Display display;
	
	public DisplayMetadataValue(Display display) {
		super();
		this.display = display;
	}
	
	@Override
	public Display value() {
		return display;
	}

}
