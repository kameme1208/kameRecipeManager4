package com.gitlab.kameme1208.kameRecipeManager.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

public class InventorySifter {

	public static Map<Character, ItemStack> startSift(ItemStack[] craft, List<String> shapes) {
		if(craft.length == 9) {
			if(isEmptyLine(craft, 0, 1, 2)) {System.arraycopy(craft, 3, craft, 0, 6);craft[6] = craft[7] = craft[8] = null;}
			if(isEmptyLine(craft, 0, 1, 2)) {System.arraycopy(craft, 3, craft, 0, 6);craft[6] = craft[7] = craft[8] = null;}
			if(isEmptyLine(craft, 0, 3, 6)) {System.arraycopy(craft, 1, craft, 0, 8);craft[2] = craft[5] = craft[8] = null;}
			if(isEmptyLine(craft, 0, 3, 6)) {System.arraycopy(craft, 1, craft, 0, 8);craft[2] = craft[5] = craft[8] = null;}
		}else {
			if(isEmptyLine(craft, 0, 1, 2)) {System.arraycopy(craft, 2, craft, 0, 2);craft[2] = craft[3] = null;}
			if(isEmptyLine(craft, 0, 2, 4)) {System.arraycopy(craft, 1, craft, 0, 3);craft[1] = craft[3] = null;}
		}
		return sortSift(craft, shapes);
	}
	

	private static boolean isEmptyLine(ItemStack[] craft, int a, int b, int c) {
		return craft[a] == null && craft[b] == null && (craft.length < 9 || craft[c] == null);
	}

	private static Map<Character, ItemStack> sortSift(ItemStack[] craft, List<String> shapes) {
		Map<Character, ItemStack> map = new HashMap<>(craft.length);
		int l = (int)Math.sqrt(craft.length);
		int i = l - 1, j = l - 1;
		while(i >= 0 && isEmptyLine(craft, l*i+0, l*i+1, l*i+2))i--;
		while(j >= 0 && isEmptyLine(craft, j+l*0, j+l*1, j+l*2))j--;
		char ch = 'a';
		for(int x = 0; x <= i; x++) {
			StringBuilder builder = new StringBuilder();
			for(int y = 0; y <= j; y++) {
				int c = x * 3 + y;
				if(craft[c] != null && craft[c].getAmount() > 0) {
					craft[c] = craft[c].clone();
					craft[c].setAmount(1);
					map.put(ch, craft[c]);
				}
				builder.append(ch);
				ch++;
			}
			shapes.add(builder.toString());
		}
		return map;
	}

}
