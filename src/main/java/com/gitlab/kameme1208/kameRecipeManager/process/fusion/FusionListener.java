package com.gitlab.kameme1208.kameRecipeManager.process.fusion;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.gitlab.kameme1208.kameRecipeManager.block.TableData;
import com.google.common.collect.Sets;

public class FusionListener implements Listener {

	@EventHandler
	private void onFusionInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();
		if(block == null || block.getType() != Material.BREWING_STAND) {
			block = player.getTargetBlock(Sets.newHashSet(), 5);
		}
		if(block.getType() != Material.BREWING_STAND)return;
		Set<String> include = TableData.fromBlock(block);
		if(include.isEmpty())return;
		
	//Recipes.findRecipe(CustomFusionRecipe.class, include, materials);
	}
	
	
}
