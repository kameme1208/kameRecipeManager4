package com.gitlab.kameme1208.kameRecipeManager.process.metadata;

import org.bukkit.metadata.MetadataValueAdapter;

import com.gitlab.kameme1208.kameRecipeManager.Main;

public abstract class KameRecipeMetadataValue extends MetadataValueAdapter {

	protected KameRecipeMetadataValue() {
		super(Main.getInstance());
	}
	
	@Override
	public void invalidate() {
	}

}
