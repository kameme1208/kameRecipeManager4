package com.gitlab.kameme1208.kameRecipeManager.process.chest;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import com.gitlab.kameme1208.kameRecipeManager.Main;

public class DoubleChestListener implements Listener {


	
	@EventHandler
	private void onInventoryClick(InventoryClickEvent event) {
		//onClick(event);
		Bukkit.getScheduler().runTask(Main.getInstance(), () -> onClick(event));
	}
	
	@EventHandler
	private void onInventoryClick(InventoryDragEvent event) {
		//onClick(event);
		Bukkit.getScheduler().runTask(Main.getInstance(), () -> onClick(event));
	}
	
	private void onClick(InventoryInteractEvent event) {
		Inventory inv = event.getView().getTopInventory();
		if(inv instanceof DoubleChestInventory && inv.getType() == InventoryType.CHEST) {
			onChestClick(event, (DoubleChestInventory)inv);
		}
	}

	private void onChestClick(InventoryEvent event, DoubleChestInventory inv) {
		Bukkit.broadcastMessage(inv != null ? (inv.getType() + " " + inv.getClass()) : "null");
	}
}
