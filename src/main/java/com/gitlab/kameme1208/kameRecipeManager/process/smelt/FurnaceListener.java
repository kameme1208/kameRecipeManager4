package com.gitlab.kameme1208.kameRecipeManager.process.smelt;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.Inventory;

import com.gitlab.kameme1208.kameRecipeManager.Main;

public class FurnaceListener implements Listener {


	
	@EventHandler
	private void onInventoryClick(InventoryClickEvent event) {
		//onClick(event);
		Bukkit.getScheduler().runTask(Main.getInstance(), () -> onClick(event));
	}
	
	@EventHandler
	private void onInventoryClick(InventoryDragEvent event) {
		//onClick(event);
		Bukkit.getScheduler().runTask(Main.getInstance(), () -> onClick(event));
	}
	
	private void onClick(InventoryInteractEvent event) {
		Inventory inv = event.getView().getTopInventory();
		if(inv instanceof FurnaceInventory && inv.getType() == InventoryType.FURNACE) {
			onFurnaceClick(event, (FurnaceInventory)inv);
		}
	}
	
	private void onFurnaceClick(InventoryEvent event, FurnaceInventory inv) {
		Bukkit.broadcastMessage(inv != null ? (inv.getType() + " " + inv.getClass()) : "null");
	}
}
