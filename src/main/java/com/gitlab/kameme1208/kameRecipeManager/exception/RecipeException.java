package com.gitlab.kameme1208.kameRecipeManager.exception;
public abstract class RecipeException extends Exception {
	public RecipeException(String str) {
		super(str);
	}

	/**
	 * このメソッドは例外のメッセージを取得するメソッドです。
	 * この例外の実装は例外のメッセージのフォーマットに基づくように実装されなければなりません。
	 *
	 * <br><b>実装例:</b><br>
	 *
	 * returns "Class名 : 例外メッセージ(ヘッダー) : getMessage() : 例外メッセージ(フッター)"
	 */
	public String getMessage() {return super.getMessage();};
}
