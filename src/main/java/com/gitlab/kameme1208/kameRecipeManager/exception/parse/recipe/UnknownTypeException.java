package com.gitlab.kameme1208.kameRecipeManager.exception.parse.recipe;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;

public class UnknownTypeException extends RecipeParseException {

	public UnknownTypeException(String str) {
		super(str);
	}

}
