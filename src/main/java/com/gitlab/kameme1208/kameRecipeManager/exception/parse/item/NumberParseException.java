package com.gitlab.kameme1208.kameRecipeManager.exception.parse.item;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;

public class NumberParseException extends RecipeParseException {

	public NumberParseException(String str) {
		super(str);
	}

	public String getMessage() {
		return "NumberParseException: " + super.getMessage() + "is Invalid character";

	}

}
