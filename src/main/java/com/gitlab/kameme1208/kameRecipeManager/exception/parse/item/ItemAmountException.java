package com.gitlab.kameme1208.kameRecipeManager.exception.parse.item;

public class ItemAmountException extends RecipeItemParseException {

	public ItemAmountException(int amount) {
		super(String.valueOf(amount));
	}

	public String getMessage() {
		return "ItemAmountException: (" + super.getMessage() + ") is too many items";

	}

}
