package com.gitlab.kameme1208.kameRecipeManager.exception.parse;

import com.gitlab.kameme1208.kameRecipeManager.exception.RecipeException;

public abstract class RecipeParseException extends RecipeException {

	public RecipeParseException(String str) {
		super(str);
	}
}
