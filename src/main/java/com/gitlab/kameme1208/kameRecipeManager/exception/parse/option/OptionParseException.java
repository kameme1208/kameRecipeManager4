package com.gitlab.kameme1208.kameRecipeManager.exception.parse.option;

public class OptionParseException extends RecipeOptionParseException {
	public final ErrorType type;

	public enum ErrorType {
		/**[kameRecipeManager] OptionParseException: can not parse item (%message%)*/
		ITEM,
		/**[kameRecipeManager] OptionParseException: arguments is not match (%message%)*/
		ARGUMENT,
		/**[kameRecipeManager] OptionParseException: can not parse number (%message%)*/
		NUMBER,
		/**[kameRecipeManager] OptionParseException: (%message%)*/
		OTHER,

		MATERIAL,

		ENCHANTMENTS,

		WARN,

		AUGMENTS,

		BADNBT
	}

	public OptionParseException(String message, ErrorType type) {
		super(message);
		this.type = type;
	}

	public String getMessage() {
		String str;
		switch(type) {
		case ARGUMENT:
			str = "EffectAugumentError: (" + super.getMessage() + ") is Invalid character";
			break;
		case ITEM:
			str = "EffectMaterialTypeError: (" + super.getMessage() + ") is Invalid character";
			break;
		case NUMBER:
			str = "EffectNumberFormatError: (" + super.getMessage() + ") is Invalid character";
			break;
		case OTHER:
			str = "その他のエラー:";
			break;
		default:
			str = "UnknownError: (" + super.getMessage() + ")";
			break;
		}
		return str;
	}


}
