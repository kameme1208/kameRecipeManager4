package com.gitlab.kameme1208.kameRecipeManager.exception.parse.option;

public class SoundParseException extends RecipeOptionParseException {

	private Type type;

	public SoundParseException(String str, Type type) {
		super(str);
		this.type = type;
	}

	public String getMessage() {
		String str;
		switch(type) {
		case ARGUMENT:
			str = "SoundParseException: AugumentError (" + super.getMessage() + ") is Invalid character";
			break;
		case TYPE:
			str = "SoundParseException: TypeFormatError (" + super.getMessage() + ") is Invalid sound tyoe";
			break;
		case NUMBER:
			str = "SoundParseException: NumberFormatError (" + super.getMessage() + ") is Invalid character";
			break;
		default:
			str = "SoundParseException: UnknownError (" + super.getMessage() + ")";
			break;
		}
		return str;
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		ARGUMENT,
		TYPE,
		NUMBER,
	}

}
