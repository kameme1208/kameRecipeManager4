package com.gitlab.kameme1208.kameRecipeManager.exception.option;

public abstract class CraftOptionException extends Exception {

	public abstract String getMessage();

}
