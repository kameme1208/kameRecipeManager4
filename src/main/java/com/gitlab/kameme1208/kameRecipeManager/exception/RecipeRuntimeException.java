package com.gitlab.kameme1208.kameRecipeManager.exception;

public abstract class RecipeRuntimeException extends RuntimeException {
	public RecipeRuntimeException(String str) {
		super(str);
	}
}
