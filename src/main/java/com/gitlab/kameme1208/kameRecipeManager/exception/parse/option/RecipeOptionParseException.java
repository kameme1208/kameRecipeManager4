package com.gitlab.kameme1208.kameRecipeManager.exception.parse.option;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;

public class RecipeOptionParseException extends RecipeParseException {

	public RecipeOptionParseException(String str) {
		super(str);
	}
}
