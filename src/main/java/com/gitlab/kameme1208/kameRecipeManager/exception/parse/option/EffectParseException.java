package com.gitlab.kameme1208.kameRecipeManager.exception.parse.option;

public class EffectParseException extends RecipeOptionParseException {

	private Type type;

	public EffectParseException(String str, Type type) {
		super(str);
		this.type = type;
	}

	public String getMessage() {
		String str;
		switch(type) {
		case ARGUMENT:
			str = "EffectParseException: AugumentError (" + super.getMessage() + ") is Invalid character";
			break;
		case MATERIAL:
			str = "EffectParseException: MaterialTypeError (" + super.getMessage() + ") is Invalid material";
			break;
		case NUMBER:
			str = "EffectParseException: NumberFormatError (" + super.getMessage() + ") is Invalid character";
			break;
		default:
			str = "EffectParseException: UnknownError (" + super.getMessage() + ")";
			break;
		}
		return str;
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		ARGUMENT,
		MATERIAL,
		NUMBER,
	}

}
