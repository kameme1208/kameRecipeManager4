package com.gitlab.kameme1208.kameRecipeManager.exception.parse.item;

public class ItemParseException extends RecipeItemParseException {

	private Type type;

	public ItemParseException(String str, Type type) {
		super(str);
		this.type = type;
	}

	public String getMessage() {
		String str;
		switch(type) {
		case ARGUMENT:
			str = "ItemParseException: AugumentError (" + super.getMessage() + ")";
			break;
		case NBT:
			str = "ItemParseException: NBTParseError (" + super.getMessage() + ") is Invalid nbt";
			break;
		case MATERIAL:
			str = "ItemParseException: MaterialTypeError (" + super.getMessage() + ") is Invalid material";
			break;
		case ITEMFLAG:
			str = "ItemParseException: ItemFlagFormatError (" + super.getMessage() + ") is Invalid flag";
			break;
		case ENCHANT:
			str = "ItemParseException: EnchantFormatError (" + super.getMessage() + ") is Invalid enchant";
			break;
		case OTHER:
			str = "ItemParseException: " + super.getMessage();
			break;
		default:
			str = "ItemParseException: UnknownError (" + super.getMessage() + ")";
			break;
		}
		return str;
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		ARGUMENT,
		ITEMFLAG,
		MATERIAL,
		ENCHANT,
		OTHER,
		NBT,
	}

}
