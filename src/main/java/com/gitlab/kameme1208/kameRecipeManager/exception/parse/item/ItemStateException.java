package com.gitlab.kameme1208.kameRecipeManager.exception.parse.item;

public class ItemStateException extends RecipeItemParseException {

	public ItemStateException(String str) {
		super(str);
	}

	@Override
	public String getMessage() {
		return "ItemStateException: " + super.getMessage() + " already set items!";
	}

}
