package com.gitlab.kameme1208.kameRecipeManager.exception.parse.item;

import com.gitlab.kameme1208.kameRecipeManager.exception.parse.RecipeParseException;

public abstract class RecipeItemParseException extends RecipeParseException {

	public RecipeItemParseException(String str) {
		super(str);
	}

}
