package com.gitlab.kameme1208.kameRecipeManager.block;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.kameme1208.kameRecipeManager.Main;
import com.gitlab.kameme1208.kameRecipeManager.utils.display.TableNameTag;

public class TableData implements Listener {

	private static Map<Block, ItemData> metas = new HashMap<>();
	private static Map<Entity, ItemData> drops = new HashMap<>();

	static {
		Bukkit.getScheduler().runTaskTimer(Main.getInstance(), () -> {
			drops.entrySet().removeIf(TableData::prepareDrop);
			Bukkit.getOnlinePlayers().forEach(TableData::prepareView);
		}, 0, 1);
	}

	@EventHandler
	private void onEnable(PluginEnableEvent event) throws IOException {
		if(event.getPlugin() instanceof Main) {
			File file = new File(Main.getInstance().getDataFolder(), "blocks.save");
			if(!file.isFile())new YamlConfiguration().save(file);
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			ConfigurationSection blocks = config.getConfigurationSection("blocks");
			if(blocks == null)return;
			for(String key : blocks.getKeys(false)) {
				try {
					String[] args = key.split("_");
					World world = Bukkit.getWorld(UUID.fromString(args[0]));
					Block block = world.getBlockAt(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
					ItemStack item = blocks.getItemStack(key);
					metas.put(block, key.endsWith("_") ?  new RecipeData(block, item) :  new ItemData(item));
				}catch (Exception e) {
					Main.getInstance().getLogger().severe("ERROR : " + key + " can not loaded");
					Main.getInstance().getLogger().severe("ERROR : val = " + blocks.get(key));
					e.printStackTrace();
				}
			}
		}
	}

	@EventHandler
	private void onDisable(PluginDisableEvent event) throws IOException {
		if(event.getPlugin() instanceof Main) {
			metas.values().forEach(ItemData::destroy);
			drops.forEach(this::remove);
			YamlConfiguration config = new YamlConfiguration();
			ConfigurationSection section = config.createSection("blocks");
			for(Entry<Block, ItemData> data : metas.entrySet()) {
				Block block = data.getKey();
				ItemData table = data.getValue();
				String key = block.getWorld().getUID() + "_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				section.set(key.concat(table instanceof RecipeData ? "_" : ""), table.item);
			}
			config.save(new File(Main.getInstance().getDataFolder(), "blocks.save"));
		}
	}

	private void remove(Entity entity, ItemData data) {
		entity.remove();
		data.drop(entity.getLocation());
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if(event.getBlockAgainst().equals(event.getBlock()) && event.getItemInHand().getType().getMaxDurability() > 0)return;
		ItemStack item = event.getItemInHand().clone();
		item.setAmount(1);
		if(item == null || item.getData().toItemStack(1).equals(item))return;
		ItemMeta meta = item.getItemMeta();
		String lore = (meta.getLore() + "").replace("§", "");
		if(lore != null && lore.contains("@craft") || lore.contains("@furnace") || lore.contains("@fusion") || lore.contains("@smash")) {
			metas.put(event.getBlock(), new RecipeData(event.getBlock(), item));
			event.getBlock().getWorld().playSound(event.getBlock().getLocation().add(0.5, 0.5, 0.5), Sound.ITEM_ARMOR_EQUIP_ELYTRA, 1, 2);
		}else {
			metas.put(event.getBlock(), new ItemData(item));
		}
		Location loc = event.getBlock().getLocation().add(0.6, 0.5, 0.6);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add( 0.6, 0, 0), 4, 0, 0.25, 0.25);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add(-0.6, 0, 0), 4, 0, 0.25, 0.25);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add( 0, 0.6, 0), 4, 0.25, 0, 0.25);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add( 0,-0.6, 0), 4, 0.25, 0, 0.25);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add( 0, 0, 0.6), 4, 0.25, 0.25, 0);
		loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add( 0, 0,-0.6), 4, 0.25, 0.25, 0);
		
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	private void onBlockPiston(BlockPistonExtendEvent event) {
		onPistonMove(event, event.getBlocks());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	private void onBlockPiston(BlockPistonRetractEvent event) {
		onPistonMove(event, event.getBlocks());
	}

	private void onPistonMove(BlockPistonEvent event, List<Block> blocks) {
		Map<Block, ItemData> list = new HashMap<>();
		BlockFace b = event.getDirection();
		for(Block block : blocks) {
			ItemData data = metas.remove(block);
			if(data == null)continue;
			switch(block.getPistonMoveReaction()) {
			case BREAK:
				destroyBlock(block, data, null);
				break;
			case MOVE:
				list.put(block.getRelative(b), data);
				if(data instanceof RecipeData) {
					((RecipeData)data).move(block.getRelative(b));
					block.getWorld().playSound(block.getLocation().add(0.5, 0.5, 0.5), Sound.ENTITY_ITEMFRAME_ROTATE_ITEM, 1, 1);
				}
				break;
			case BLOCK:
			default:
				break;
			}
		}
		metas.putAll(list);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	private void onBlockBreak(EntityExplodeEvent event) {
		for(Block block : event.blockList()) {
			ItemData data = metas.remove(block);
			if(data != null)destroyBlock(block, data, null);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	private void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		ItemData data = metas.remove(block);
		if(data == null)return;
		destroyBlock(block, data, event.getPlayer());
		Location loc = block.getLocation();
		for(Player player : block.getWorld().getPlayers()) {
			if(player != event.getPlayer() && player.getLocation().distance(loc) < 16)player.playEffect(loc, Effect.STEP_SOUND, block.getState().getData());
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockChange(EntityChangeBlockEvent event) {
		Entity entity = event.getEntity();
		if(entity instanceof FallingBlock) {
			if(!entity.isOnGround()) {
				ItemData data = metas.remove(event.getBlock());
				if(data == null)return;
				((FallingBlock) entity).setDropItem(false);
				if(data instanceof RecipeData)((RecipeData)data).nametag.ride(entity);
				drops.put(entity, data);
			}else {
				ItemData data = drops.remove(entity);
				if(data == null)return;
				metas.put(event.getBlock(), data);
				if(data instanceof RecipeData) {
					entity.getWorld().playSound(entity.getLocation(), Sound.ENTITY_ARMORSTAND_HIT, 1, 1);
					((RecipeData)data).nametag.moveTo(event.getBlock());
				}
			}
		}else {
			if(metas.get(event.getBlock()) == null)return;
			event.setCancelled(true);
			event.getBlock().getState().update();
		}
	}

	private void destroyBlock(Block block, ItemData data, Player player) {
		if(block.getWorld().getGameRuleValue("doTileDrops").equals("true"))data.drop(block, player);
		if(data instanceof RecipeData) {
			block.getWorld().playSound(block.getLocation().add(0.5, 0.5, 0.5), Sound.ENTITY_ITEMFRAME_BREAK, 1, 1);
		}
		block.setType(Material.AIR);
	}

	private static boolean prepareDrop(Entry<Entity, ItemData> entry) {
		if(!entry.getKey().isDead())return false;
		World world = entry.getKey().getWorld();
		Location loc = entry.getKey().getLocation();
		if(world.getGameRuleValue("doTileDrops").equals("true"))entry.getValue().drop(loc);
		if(entry.getValue() instanceof RecipeData)world.playSound(loc, Sound.ENTITY_ARMORSTAND_FALL, 1, 1);
		return true;
	}

	private static Map<Player, Set<RecipeData>> view = new HashMap<>();

	private static void prepareView(Player player) {
		for(Entry<Block, ItemData> entry : metas.entrySet()) {
			if(entry.getValue() instanceof RecipeData) {
				if(view.getOrDefault(player, Collections.emptySet()).contains(entry.getValue())) {
					if(!entry.getKey().getWorld().equals(player.getWorld()) || entry.getKey().getLocation().distance(player.getLocation()) > 16) {
						Set<RecipeData> data = view.getOrDefault(player, new HashSet<>());
						data.remove((RecipeData)entry.getValue());
						view.putIfAbsent(player, data);
						((RecipeData)entry.getValue()).nametag.hide(player);
					}
				}else {
					if(entry.getKey().getWorld().equals(player.getWorld()) && entry.getKey().getLocation().distance(player.getLocation()) < 16) {
						Set<RecipeData> data = view.getOrDefault(player, new HashSet<>());
						data.add((RecipeData)entry.getValue());
						view.putIfAbsent(player, data);
						((RecipeData)entry.getValue()).nametag.show(player);
					}
				}
			}
		}
	}

	public static Set<String> fromBlock(Block block) {
		RecipeData value = getData(block, RecipeData.class);
		return value != null ? new LinkedHashSet<>(value.includeName) : new LinkedHashSet<>();
	}

	public static Set<String> fromItem(ItemStack item) {
		ItemMeta im = item.getItemMeta();
		String name = im.getDisplayName();
		Set<String> set = new LinkedHashSet<>();
		if(name != null)set.add(name);
		if(im.hasLore())for(String lore : im.getLore()) {
			if(lore.toLowerCase().startsWith("@include:"))set.add(lore.substring(9));
		}
		return set;
	}

	public static String getName(Block block, String def) {
		RecipeData value = getData(block, RecipeData.class);
		return value != null ? value.tableName : def;
	}

	public static ItemStack getItem(Block block, ItemStack def) {
		ItemData value = getData(block, ItemData.class);
		return value != null ? value.item : def;
	}

	private static <T extends ItemData> T getData(Block block, Class<T> clazz) {
		ItemData value = metas.get(block);
		return clazz.isInstance(value) ? clazz.cast(value) : null;
	}

	private class ItemData {
		private ItemStack item;
		ItemData(ItemStack item) {
			this.item = item;
		}

		public void drop(Block block, Player player) {
			if(player == null || (!player.getGameMode().equals(GameMode.CREATIVE) && !block.getDrops(player.getEquipment().getItemInMainHand()).isEmpty()))
				block.getWorld().dropItemNaturally(block.getLocation().add(0.5, 0.5, 0.5), item);
			item = null;
		}

		public void drop(Location loc) {
			loc.getWorld().dropItemNaturally(loc, item);
			destroy();
		}

		public void destroy() {
			item = null;
		}
	}

	private class RecipeData extends ItemData {
		private String tableName;
		private Set<String> includeName = new LinkedHashSet<>();
		private TableNameTag nametag;
		RecipeData(Block block,ItemStack item) {
			super(item);
			ItemMeta im = item.getItemMeta();
			String name = im.getDisplayName();
			if(name != null)includeName.add(name);
			if(im.hasLore())for(String lore : im.getLore()) {
				if(lore.toLowerCase().startsWith("@include:"))includeName.add(lore.substring(9));
			}
			nametag = TableNameTag.create(block, name);
		}

		public void move(Block to) {
			nametag.moveTo(to);
		}

		@Override
		public void drop(Block block, Player player) {
			super.drop(block, player);
			destroy();
		}

		@Override
		public void drop(Location loc) {
			super.drop(loc);
			destroy();
		}
		
		@Override
		public void destroy() {
			tableName = null;
			includeName = null;
			nametag.destroy();
			nametag = null;
		}
	}
}
